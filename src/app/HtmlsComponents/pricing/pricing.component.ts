import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppService } from 'src/app/app.service';
import {PlanFeature} from 'src/models/Pricing';
import { ToastrManager } from 'ng6-toastr-notifications';
import { NgxSpinnerService } from 'ngx-spinner';
declare var $: any;
@Component({
    selector: 'pricing',
    templateUrl: './pricing.component.html',
    providers: [AppService],
    styleUrls: ['./pricing.css']
})
export class pricingComponent implements OnInit {
    Images:any=[];
    Plans:PlanFeature[] = [];
    Enquireform:any;
    afterRequest: any = false;
    emailPattern = '[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}';
    constructor( private route: ActivatedRoute, private fb: FormBuilder, private toastr:ToastrManager,
        private router: Router, private appService: AppService, private spinner: NgxSpinnerService)
        {

        }
    ngOnInit() {
      this.GetPlans();
      this.Enquireform = this.fb.group({
        FirstName: ['', Validators.compose([Validators.required])],  
        LastName: ['', Validators.compose([Validators.required])],
        Email: ['', Validators.compose([Validators.required, Validators.email])],   
        FollowUPBy: ['', Validators.compose([Validators.required])],
        EnquireOn : ['', Validators.compose([Validators.nullValidator])]
      });
    }

    clear()
    {
      this.Enquireform.reset();
    }

    enquireNow() { 
      if (this.Enquireform.invalid) {
        this.Enquireform.controls.FirstName.markAsTouched();
        this.Enquireform.controls.LastName.markAsTouched();
        this.Enquireform.controls.Email.markAsTouched();
        this.Enquireform.controls.FollowUPBy.markAsTouched();
        this.toastr.errorToastr('Please provide the valid details!!', 'Oops!');
        setTimeout(() => {
            this.toastr.dismissToastr;
        }, 3000);
  
      }
       else 
       {
         this.Enquireform.value.EnquireOn = new Date();
          this.appService.EnquireNow(this.Enquireform.value)
          .subscribe(
          data => {  
            this.afterRequest = true;  
            this.Enquireform.reset();    
            //this.toastr.successToastr('Thank you for Choosing Enterprise, our sales team will get back to you shortly.', 'Success!');
         }
          
        );
      }
  }

  Demo()
  {
    this.Enquireform.reset();
    this.afterRequest = false;
  }

    signUp(id)
    {
        sessionStorage.setItem('planId', id);
        this.router.navigateByUrl('customersignup'); 
    }

    GetPlans()
    {
      this.spinner.show();
        this.appService.GetPlans()
        .subscribe(
          data => {
            this.Plans = data;
            this.spinner.hide();
          });
    }
}