import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
declare var $: any;
@Component({
	selector: 'Freelancerecruiters',
	templateUrl: './freelance-recruiters.component.html',
})
export class FreelancerecruitersComponent implements OnInit {
	Images:any=[];
	optionSlide = {
		items: 3,
		dots: true,
		nav: false,
		margin: 20,
		loop: true,
		autoPlay: 3000,
		dotsContainer: '#carousel-custom-dots',
		responsive: {
			// breakpoint from 0 up
			0: {
				items: 1,
			},
			// breakpoint from 480 up
			480: {
				items: 1,
			},
			768: {
				items: 2
			},
			// breakpoint from 768 up
			1023: {
				items: 3
			}
		}
	};
	slideOption = {
    items: 3,
    dots: true,
    nav: false,
    margin: 20,
    loop: true,
    autoPlay: 3000,
    dotsContainer: '#one-stop-dots',
    responsive: {
      // breakpoint from 0 up
      0: {
        items: 1,
      },
      // breakpoint from 480 up
      480: {
        items: 1,
      },
      768: {
        items: 2
      },
      // breakpoint from 768 up
      1023: {
        items: 3
      }
    }
  };
	stepsSlide = [
		{
			id: "20",
			descritpition: 'Our all-inclusive platform eliminates the complexity of using multiple tools and increases hiring manager productivity.'
		},
		{
			id: "21",
			descritpition: 'Upload your candidates by creating a profile for each one andArytic will then ask the candidate’s permission to be represented, this allows you to act on the candidate’s behalf'
		},
		{
			id: "22",
			descritpition: 'Once the candidates accept, you will be able to browse through open jobs and will be able to submit your candidates to the best positions.'
		},
		{
			id: "23",
			descritpition: 'Connect directly with top employers without becoming an approved vendor, you will have accessto see all open jobs posted on Arytic.'
		},
		{
			id: "24",
			descritpition: 'If your candidate is hired through Arytic, you will receive a placement payment based on the salary of the job filled.'
		}
	]


	oneShopSlide = [
		{

			id: "16",
			title: 'Place more candidates',
			descritpition: 'See job posts from all employers and instantly see which jobs to pursue for each candidate.'
		},
		{
			id: "17",
			title: 'Keep your candidates in the loop.',
			descritpition: 'Easily send feedback when hiring managers update job status.'
		},
		{
			id: "18",
			title: 'Keep more money.',
			descritpition: 'Pay a very low monthly fee and a 7% rate for each successful hire. '
		},
		{
			id: "19",
			title: 'Eliminate busy work.',
			descritpition: 'Spend more time nurturing your candidate and employer relationships by letting Arytic do the busy work.'
		}
	]

	constructor(private route: ActivatedRoute, private router: Router) { }
	ngOnInit() {
		$(function () {
			$(document).ready(function () {
				$('.shop__diagaram .shop__icon').each(function () {
					$(this).mouseenter(function () {
						var dataId = $(this).attr('data-id');
						$('.shop__info').hide();
						$('#' + dataId).show();
					});
				})
			});
		});
	}
}