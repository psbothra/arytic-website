import { Component, ViewContainerRef, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { ToastrManager } from 'ng6-toastr-notifications';
import { AppService } from '../../app.service';
import { SettingsService } from 'src/settings/settings.service';
declare var $: any;
@Component({
  selector: 'insights',
  templateUrl: './insights.component.html',
})
export class insightsComponent implements OnInit {
  Subscribeform: FormGroup;
  WhitePaperform: FormGroup;
  whitepaper: any;
  demoform: FormGroup;
  res: any;
  show: any = false;
  afterRequest: any = false;
  emailPattern = '^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$';
  whitepaperpath: any;
  result: any;
  constructor(private route: ActivatedRoute, private toastr: ToastrManager, private _vcr: ViewContainerRef,
    private fb: FormBuilder, private router: Router, private appService: AppService, private settingsService: SettingsService) {

  }
  Send() {
    if (!this.Subscribeform.valid) {
      this.Subscribeform.controls.ToEmailID.markAsTouched();
      this.toastr.errorToastr('Please provide the valid details!!', 'Oops!');
      setTimeout(() => {
        this.toastr.dismissToastr;
      }, 3000);
    }
    else {
      this.appService.AryticSubscription(this.Subscribeform.value)
        .subscribe(
          data => {
            this.result = data;
            if (this.result == 0) {
              this.toastr.successToastr('Thank you for Subscribing Arytic', 'Success!');
              setTimeout(() => {

                this.toastr.dismissToastr;
                this.Subscribeform.reset();
              }, 3000);

            }
          }

        );
    }
  }

  cancelRequest() {
    this.show = false;
    this.afterRequest = false;
    this.demoform.reset();
  }




  DemoRequest() {
    this.show = false;
    this.afterRequest = false;
    if (!this.demoform.valid) {
      this.demoform.controls['FirstName'].markAsTouched()
      this.demoform.controls['LastName'].markAsTouched()
      this.demoform.controls['EmailId'].markAsTouched()
    }
    else {
      this.appService.DemoRequest(this.demoform.value)
        .subscribe(
          data => {
            this.res = data;
            if (this.res == 0) {
              this.afterRequest = true;
            }
            else if (this.res == 1) {
              this.show = true;
            }
          },
          error => {
            this.demoform.reset();
          },
          () => console.log('Call Sucessfull')
        );
    }
  }

  WhitePaper(val) {
    this.whitepaper = val;
  }

  Demo() {
    this.show = false;
    this.afterRequest = false;
    this.ngOnInit();
  }

  EmailClear() {
    this.show = false;
  }

  SaveWhitepaper() {
    if (!this.WhitePaperform.valid) {
      this.WhitePaperform.controls.UserName.markAsTouched();
      this.WhitePaperform.controls.CompanyName.markAsTouched();
      this.WhitePaperform.controls.EmailId.markAsTouched();
      this.toastr.errorToastr('Please provide the valid details!!', 'Oops!');
      setTimeout(() => {
        this.toastr.dismissToastr;
      }, 3000);
    }
    else {
      if (this.whitepaper == 1) {
        this.whitepaperpath = 'Whitepaper-1';
        this.WhitePaperform.value.Body = this.settingsService.settings.Whitepaper1;
      }
      else if (this.whitepaper == 2) {
        this.whitepaperpath = 'Whitepaper-2';
        this.WhitePaperform.value.Body = this.settingsService.settings.Whitepaper2;
      }
      this.WhitePaperform.value.WhitepaperTitle = this.whitepaperpath;
      this.appService.AryticWhitepaper(this.WhitePaperform.value)
        .subscribe(
          data => {
            this.result = data;
            if (this.result == 0) {
              this.WhitePaperform.reset();
              $("#blogDownload").modal("hide");
              this.toastr.successToastr('Download Link sent to your email !!', 'Success');
              setTimeout(() => {
                this.toastr.dismissToastr;
              }, 3000);
              $("#exampleModal").modal("show");
            }
          }

        );
    }
  }
  ngOnInit() {

    /* getting-url-hash-location */
    var hash_value = window.location.hash;
    if (hash_value) {
      var hash_id = hash_value + ".hidden-blog";
      $(".hide-cards").hide();
      $(hash_id).show();
    }
    /* #getting-url-hash-location */





    this.show = false;
    this.afterRequest = false;
    this.demoform = this.fb.group({
      'GuestUserId': [0, Validators.compose([Validators.required])],
      'FirstName': ['', Validators.compose([Validators.required])],
      'LastName': ['', Validators.compose([Validators.required])],
      'EmailId': ['', Validators.compose([Validators.required, Validators.email])],
      'PhoneNumber': ['', Validators.compose([Validators.nullValidator, Validators.minLength(10)])],
      'IsDemoRequest': [true, Validators.compose([Validators.nullValidator])],
      'GuestRoleId': ['', Validators.compose([Validators.nullValidator])],
    });

    this.Subscribeform = this.fb.group({
      'ToEmailID': ['', Validators.compose([Validators.required])],
    });

    this.WhitePaperform = this.fb.group({
      'UserName': ['', Validators.compose([Validators.nullValidator])],
      'CompanyName': ['', Validators.compose([Validators.nullValidator])],
      'EmailId': ['', Validators.compose([Validators.nullValidator])],
      'ContactNumber': ['', Validators.compose([Validators.nullValidator, Validators.minLength(10)])],
      'WhitepaperTitle': ['', Validators.compose([Validators.nullValidator])],
      'IsDownloaded': [true, Validators.compose([Validators.nullValidator])],
      'Body': ['', Validators.compose([Validators.nullValidator])]
    });
    $(document).ready(function () {
      $('.gallery').mauGallery({
        columns: {
          xs: 1,
          sm: 2,
          md: 3,
          lg: 3,
          xl: 3
        },
        lightBox: true,
        lightboxId: 'myAwesomeLightbox',
        showTags: true,
        tagsPosition: 'top'
      });

      $('.card-body .news-view').click(function () {
        $('.hide-cards').hide();
        var t = $(this).attr('news-tab');
        $('.hidden-blog').hide();
        $('#' + t).show();
      });
      $(".goback").click(function () {
        $('.hidden-blog').hide();
        $('.hide-cards').show();
      })




    });


    /**** */

    var url = $("#ourVideo").attr('src');

    $("#videoModalCenter").on('hide.bs.modal', function () {
      $("#ourVideo").attr('src', '');
    });

    $("#videoModalCenter").on('show.bs.modal', function () {
      $("#ourVideo").attr('src', url);
    });

     /** */
  setTimeout(function () {
    // Do something after 1 second 
    $(function () {
      //var current = window.location.hash.substr(1);; 
      //alert(current); 
      var a = location.href;
      var b = a.substring(a.indexOf("?") + 1);
      var c = b.split('=')[1]

      $('.nav li a').each(function () {
        $("a[data-images-toggle='" + c + "']").trigger("click");
      })

    })
  }, 1000);

  function Blogs() {
    $("a[data-images-toggle='Blogs']").trigger("click").addClass("active-tag active");
  }
  function Whitepapers() {
    $("a[data-images-toggle='Whitepapers']").trigger("click").addClass("active-tag active");
  }
  function NewsEvents() {
    $("a[data-images-toggle='News']").trigger("click").addClass("active-tag active");
  }
  $('#Blogs').click(function () {
    Blogs();
  });
  $('#Whitepapers').click(function () {
    Whitepapers();
  });
  $('#News').click(function () {
    NewsEvents();
  });
  /** */
  }
}