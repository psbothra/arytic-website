import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
declare var $: any;
@Component({
	selector: 'candidate',
	templateUrl: './candidate.component.html',
	styleUrls: ['./../customer/customer.component.css']
})
export class candidateComponent implements OnInit {
	Images:any=[];
	optionSlide = {
		items: 3,
		dots: true,
		nav: false,
		margin: 20,
		loop: true,
		autoPlay: 3000,
		dotsContainer: '#carousel-custom-dots',
		responsive: {
			// breakpoint from 0 up
			0: {
				items: 1,
			},
			// breakpoint from 480 up
			480: {
				items: 1,
			},
			768: {
				items: 2
			},
			// breakpoint from 768 up
			1023: {
				items: 3
			}
		}
	};

	stepsSlide = [
		{

			id: "11",
			title: 'One-stop shop',
			descritpition: 'Our all-inclusive platform eliminates the complexity of using multiple tools and increases hiring manager productivity.'
		},
		{
			id: "12",
			title: 'Reduce time to hire.',
			descritpition: 'Build your perfect team faster by finding top talent, with Arytic, you can cut your time to hire from a month down to a week.'
		},
		{
			id: "13",
			title: 'Increase retention rates.',
			descritpition: 'Reduce attrition and avoid having to re-fill the same position repeatedly. There is a perfect fit for even the most challenging jobs, Arytic can help you find it.'
		},
		{
			id: "14",
			title: 'Hire the perfect fit.',
			descritpition: 'Go beyond skills and experience, hire based on five characteristics and be confident that your team will work well together.'
		},
		{
			id: "15",
			title: 'Promote social justice.',
			descritpition: 'Build a more inclusive team and avoid unconscious bias by reviewing applications from qualified candidates without regards to their identity.'
		}
	]

	oneShopSlide = [
		{

			id: "6",
			title: 'One-stop shop',
			descritpition: 'Our all-inclusive platform eliminates the complexity of using multiple tools and increases hiring manager productivity.'
		},
		{
			id: "7",
			title: 'Reduce time to hire.',
			descritpition: 'Build your perfect team faster by finding top talent, with Arytic, you can cut your time to hire from a month down to a week.'
		},
		{
			id: "8",
			title: 'Increase retention rates.',
			descritpition: 'Reduce attrition and avoid having to re-fill the same position repeatedly. There is a perfect fit for even the most challenging jobs, Arytic can help you find it.'
		},
		{
			id: "9",
			title: 'Hire the perfect fit.',
			descritpition: 'Go beyond skills and experience, hire based on five characteristics and be confident that your team will work well together.'
		},
		{
			id: "10",
			title: 'Promote social justice.',
			descritpition: 'Build a more inclusive team and avoid unconscious bias by reviewing applications from qualified candidates without regards to their identity.'
		}
	]
	constructor(private route: ActivatedRoute, private router: Router) { }
	ngOnInit() {
		$(function () {
			$(document).ready(function () {
				$('.shop__diagaram .shop__icon').each(function () {
					$(this).mouseenter(function () {
						var dataId = $(this).attr('data-id');
						$('.shop__info').hide();
						$('#' + dataId).show();
					});
				})
			});
		});
	}
}