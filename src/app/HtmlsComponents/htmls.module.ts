import { NgModule } from '@angular/core';
import { CommonModule} from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RoutesModule } from './auth-routes.module';
import { SharedModule } from '../shared/shared.module';
import {HomeComponent} from './home/home.component';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {AboutComponent} from './aboutus/about.component';
import {LayoutComponent} from './layout.component';
import {candidateComponent} from './candidate/candidate.component';
import {customerComponent} from './customer/customer.component';
import {contactComponent} from './contactus/contactus.component';
import {disclaimerComponent} from './disclaimer/disclaimer.component';
import {FeatureAIHiringPredictorComponent}from './Feature-AIHiringPredictor/Feature-AIHiringPredictor.component';
import { featureSearchComponent } from './Feature-search/Feature-search.component';
import { FeatureHiringBonusComponent } from './Feature-HiringBonus/Feature-HiringBonus.component';
import { FeatureAssessandRepostComponent } from './Feature-AssessandRepost/Feature-AssessandRepost.component';
import { FeatureAnalyticallyGuidedPostComponent } from './Feature-AnalyticallyGuidedPost/Feature-AnalyticallyGuidedPost.component';
import { FeatureImpeccableProfileComponent } from './Feature-ImpeccableProfile/Feature-ImpeccableProfile.component';
import { FeatureIntelligentJobMatchMachineComponent } from './Feature-IntelligentJobMatchMachine/Feature-IntelligentJobMatchMachine.component';
import { FeatureManageBackgroundverificationReportsComponent } from './Feature-ManageBackgroundverificationReports/Feature-ManageBackgroundverificationReports.component';
import { FeatureMLRAInteractiveComponent } from './Feature-MLRA-Interactive-Application/Feature-MLRA-Interactive-Application.component';
import { FeatureNEXTGENInterviewPlatformComponent } from './Feature-NEXTGENInterviewPlatform/Feature-NEXTGENInterviewPlatform.component';
import { FeatureNotaDashboardComponent } from './Feature-NotaDashboard/Feature-NotaDashboard.component';
import { FeatureNotaJobSearchComponent } from './Feature-NotaJobSearch/Feature-NotaJobSearch.component';
import { FeatureNotJustaPostComponent } from './Feature-NotJustaPost/Feature-NotJustaPost.component';
import { FeaturePersonalityTestComponent } from './Feature-PersonalityTest/Feature-PersonalityTest.component';
import { FeaturePowerofDataScienceComponent } from './Feature-PowerofDataScience/Feature-PowerofDataScience.component';
import { FeatureRealTimeDataComponent } from './Feature-RealTimeData/Feature-RealTimeData.component';
import { FeatureSocialReferralPlatformComponent } from './Feature-SocialReferralPlatform/Feature-SocialReferralPlatform.component';
import { FeatureSuggestedJobsComponent } from './Feature-SuggestedJobs/Feature-SuggestedJobs.component';
import { FeatureTransparentJobComponent } from './Feature-TransparentJob/Feature-TransparentJob.component';
import { FeatureUniqueExperiencePlatformComponent } from './Feature-UniqueExperiencePlatform/Feature-UniqueExperiencePlatform.component';
import { FeatureUniqueProfileComponent } from './Feature-UniqueProfile/Feature-UniqueProfile.component';
import { FeatureVideoSizzleComponent } from './Feature-VideoSizzle/Feature-VideoSizzle.component';
import { featuresdetailComponent } from './features-detail/features-detail.component';
import { PrivacyPolicyComponent } from './PrivacyPolicy/PrivacyPolicy.component';
import { insightsComponent } from './insights/insights.component';
import { insightsdetailedComponent } from './insights-detailed-page/insights-detailed-page.component';
import { newsandeventsComponent } from './news-and-events/news-and-events.component';
import {CollisionComponent} from './news-and-events/Arytic-Collision/collision.component';
import {SBDCComponent } from './news-and-events/Arytic-SBDC/SBDC.component';
import {SXSWComponent } from './news-and-events/Arytic.SXSW/SXSW.component';
import { pricingComponent } from './pricing/pricing.component';
import {HiringagencysComponent} from './Hiring-Agency/hiringagency.component';
import {FreelancerecruitersComponent} from './Freelance-Recruiters/freelance-recruiters.component';
import { underconstructionComponent } from './under-construction/under-construction.component';
import { ToastrModule } from 'ng6-toastr-notifications';
import { HttpClientModule } from '@angular/common/http';
import { AppService } from '../app.service';
import { NgxSpinnerModule } from 'ngx-spinner';
import {NgxMaskModule} from 'ngx-mask';
// import {AIComponent} from './insights/Artificial Intelligence/AI.component';
// import {BigdataComponent} from './insights/Bigdata/Bigdata.component';
// import {BlockchainComponent} from './insights/Blockchain/Blockchain.component';
// import {CybersecurityComponent}from './insights/Cybersecurity/Cybersecurity.component'
// import {IoTComponent} from './insights/IoT/IoT.component';
import { OwlModule } from 'ngx-owl-carousel';
import { UsersListComponent } from './users-list/users-list.component';

@NgModule({
  imports: [
    CommonModule,
    RoutesModule,
    FormsModule, ReactiveFormsModule, SharedModule,NgxSpinnerModule,OwlModule,
  ToastrModule.forRoot(), HttpClientModule,NgxMaskModule.forRoot()
  ],
  declarations: [LayoutComponent,HomeComponent,HeaderComponent,FooterComponent,AboutComponent,
    disclaimerComponent,contactComponent,customerComponent,candidateComponent,featureSearchComponent,
    FeatureNotaJobSearchComponent,
    PrivacyPolicyComponent,
    insightsComponent,CollisionComponent,SBDCComponent,SXSWComponent,
    insightsdetailedComponent,
    newsandeventsComponent,
    pricingComponent,
    HiringagencysComponent,
    FreelancerecruitersComponent,
    underconstructionComponent,
    FeatureNotJustaPostComponent,
    FeaturePersonalityTestComponent,
    FeaturePowerofDataScienceComponent,
    FeatureRealTimeDataComponent,
    FeatureSocialReferralPlatformComponent,
    FeatureSuggestedJobsComponent,
    FeatureTransparentJobComponent,
    FeatureUniqueExperiencePlatformComponent,
    FeatureUniqueProfileComponent,
    FeatureVideoSizzleComponent,
    featuresdetailComponent,
     FeatureNEXTGENInterviewPlatformComponent, 
    FeatureMLRAInteractiveComponent, 
    FeatureManageBackgroundverificationReportsComponent,FeatureNotaDashboardComponent,
    FeatureIntelligentJobMatchMachineComponent, FeatureImpeccableProfileComponent, 
    FeatureHiringBonusComponent, FeatureAssessandRepostComponent, FeatureAnalyticallyGuidedPostComponent,   
    FeatureAIHiringPredictorComponent, UsersListComponent],
  providers: [AppService]
})
export class HtmlsModule { }