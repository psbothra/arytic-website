import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from 'src/app/app.service';
declare var $: any;
@Component({
  selector: 'customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class customerComponent implements OnInit {


  constructor( private route: ActivatedRoute, private fb: FormBuilder, 
    private router: Router, private appService: AppService) { }
Images:any=[];
demoform: FormGroup;
res:any;
show:any = false;
afterRequest: any = false;
  optionSlide = {
    items: 3,
    dots: true,
    nav: false,
    margin: 20,
    loop: true,
    autoPlay: 3000,
    dotsContainer: '#carousel-custom-dots',
    responsive: {
      // breakpoint from 0 up
      0: {
        items: 1,
      },
      // breakpoint from 480 up
      480: {
        items: 1,
      },
      768: {
        items: 2
      },
      // breakpoint from 768 up
      1023: {
        items: 3
      }
    }
  };
  slideOption = {
    items: 3,
    dots: true,
    nav: false,
    margin: 20,
    loop: true,
    autoPlay: 3000,
    dotsContainer: '#one-stop-dots',
    responsive: {
      // breakpoint from 0 up
      0: {
        items: 1,
      },
      // breakpoint from 480 up
      480: {
        items: 1,
      },
      768: {
        items: 2
      },
      // breakpoint from 768 up
      1023: {
        items: 3
      }
    }
  };
  oneShopSlide = [
    {
      title: 'One-stop shop',
      descritpition: 'Our all-inclusive platform eliminates the complexity of using multiple tools and increases hiring manager productivity.'
    },
    {
      title: 'Reduce time to hire.',
      descritpition: 'Build your perfect team faster by finding top talent, with Arytic, you can cut your time to hire from a month down to a week.'
    },
    {
      title: 'Increase retention rates.',
      descritpition: 'Reduce attrition and avoid having to re-fill the same position repeatedly. There is a perfect fit for even the most challenging jobs, Arytic can help you find it.'
    },
    {
      title: 'Hire the perfect fit.',
      descritpition: 'Go beyond skills and experience, hire based on five characteristics and be confident that your team will work well together.'
    },
    {
      title: 'Promote social justice.',
      descritpition: 'Build a more inclusive team and avoid unconscious bias by reviewing applications from qualified candidates without regards to their identity.'
    }
  ]
  Demo()
  {
    this.show = false;
    this.afterRequest= false;
    this.ngOnInit();
  }
  funnelSlide = [
    {
      title: 'One-stop shopping for your hiring needs.',
      descritpition: 'Arytic is a comprehensive suite of services that will allow you to unsubscribe and stop paying multiple HR technology platforms . Instead of logging into multiple sites, you will be able to login onto platform, see our pricing list to find the services that best fit your needs..'
    },
    {
      title: 'Job Aggregators',
      descritpition: 'Arytic is building an extensive job post database that rivals that of Indeed ™ or CareerBuilder™. As an aggregator, it collects and organizes relevant job information so that direct applicants can browse through open positions.'
    },
    {
      title: 'Applicant Tracking System',
      descritpition: 'At least half of all companies use ATS to pre-screen candidates and find the right fit, our top competitors include Bullhorn and Lever. Arytic goes above and beyond and screens candidates  based on five sets of metrics to increase your chances of finding the perfect fit.'
    },
    {
      title: 'Recruitment CRM',
      descritpition: 'The first interaction that employees have with companies is the application process; that is why it is important to create a positive experience for candidates. Our candidate relationship management tools helps you keep candidates informed and engaged throughout the process.'
    },
    {
      title: 'Background and Reference Checks',
      descritpition: 'Once you have narrowed down your top candidates, we want you to have peace of mind that you have verified information, that is why we offer background and reference checks through the portal, eliminating yet another hurdle to hiring.'
    },
    {
      title: 'Testing and Assessments',
      descritpition: 'We recognize that having years of experience does not necessarily mean that a candidate has the skills or abilities you are looking for in a team member, we make it easy for you to assign pre-hire assessments that show you a more complete picture.'
    }
  ]

  ngOnInit() {
    this.show = false;
    this.afterRequest= false;
    this.demoform = this.fb.group({
      'GuestUserId':[0, Validators.compose([Validators.required])],
      'FirstName': ['', Validators.compose([Validators.required])],
      'LastName': ['', Validators.compose([Validators.required])],
      'EmailId': ['', Validators.compose([Validators.required, Validators.email])],
      'PhoneNumber': ['',  Validators.compose([Validators.nullValidator, Validators.minLength(10)])],
      'IsDemoRequest': [true, Validators.compose([Validators.nullValidator])],
      'GuestRoleId': ['', Validators.compose([Validators.nullValidator])],
    });
    $(function () {
      $(document).ready(function () {
        $('.shop__diagaram .shop__icon').each(function () {
          $(this).mouseenter(function () {
            var dataId = $(this).attr('data-id');
            $('.shop__info').hide();
            $('#' + dataId).show();
          });
        })
        $('.revamp__funnel__section .funnel__strip').each(function () {
          $(this).mouseenter(function () {
            var dataId = $(this).attr('data-id');
            $('.funnel__info').hide();
            $('#' + dataId).show();
          });
        })
      });
    });
  }
  cancelRequest()
  {
    this.show = false;
    this.afterRequest= false;
    this.demoform.reset();
  }
  DemoRequest()
  {
    this.show = false;
    this.afterRequest= false;
    if(!this.demoform.valid)
    {
      this.demoform.controls['FirstName'].markAsTouched()
      this.demoform.controls['LastName'].markAsTouched()
      this.demoform.controls['EmailId'].markAsTouched()
    }
    else
    {
    this.appService.DemoRequest(this.demoform.value)
    .subscribe(
      data => {
        this.res = data;
        if(this.res == 0 )
        {
          this.afterRequest=true;
        }
        else if (this.res == 1 )
        {
          this.show = true;
        }
      },
      error => {
        this.demoform.reset();
      },
      () => console.log('Call Sucessfull')
    );
    }
  }
}