import { Component, OnInit } from '@angular/core';
import { Router, RouterLinkActive } from '@angular/router';
declare var $: any;



@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
})
export class FooterComponent implements OnInit {
    getCurrentYear: any;
    constructor(private router: Router) { }
    ngOnInit() {
        this.getCurrentYear = (new Date()).getFullYear();
    }
}
  /** */
  setTimeout(function () {
    // Do something after 1 second 
    $(function () {
      //var current = window.location.hash.substr(1);; 
      //alert(current); 
      var a = location.href;
      var b = a.substring(a.indexOf("?") + 1);
      var c = b.split('=')[1]

      $('.nav li a').each(function () {
        $("a[data-images-toggle='" + c + "']").trigger("click");
      })

    })
  }, 1000);

  function Blogs() {
    $("a[data-images-toggle='Blogs']").trigger("click").addClass("active-tag active");
  }
  function Whitepapers() {
    $("a[data-images-toggle='Whitepapers']").trigger("click").addClass("active-tag active");
  }
  function NewsEvents() {
    $("a[data-images-toggle='News']").trigger("click").addClass("active-tag active");
  }
  $('#Blogs').click(function () {
    Blogs();
  });
  $('#Whitepapers').click(function () {
    Whitepapers();
  });
  $('#News').click(function () {
    NewsEvents();
  });
  /** */
