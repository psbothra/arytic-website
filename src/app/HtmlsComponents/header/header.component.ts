import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ɵangular_packages_router_router_a } from '@angular/router';
import { Location } from "@angular/common";
declare var $: any;

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {
    homepage: any = false;
    constructor(private router: Router , location: Location, private activatedRoute: ActivatedRoute  ) { }
    ngOnInit() {
    //Mobile Nav close and redirectig for now
        if (location.pathname === '/' || this.router.routerState.snapshot.url === '/') {
            this.homepage = true;
        }

        $(function(){ 
            var navMain = $(".navbar-collapse");
            // navMain.on("click", "a:not([data-toggle])", null, function () {
            //     navMain.collapse('hide');
            // });
            $("body").addClass("fixed");
            $(".b-menu").addClass("fixed");
            $(window).scroll(function(){
                if($(window).scrollTop() < 50) {
                    $("body").addClass("fixed");
                    $(".b-menu").addClass("fixed");
                }
                else {
                    $("body").removeClass("fixed");
                    $(".b-menu").removeClass("fixed");
                }
            });
        });
        $('.navbar-collapse a').click(function(){
            $("#navbarTogglerDemo01").removeClass('show');
        });
        $(".scrollTo").on('click', function(e) {
            e.preventDefault();
            var target = $(this).attr('href');
            $('html, body').animate({
              scrollTop: ($(target).offset().top)
            }, 2000);
         });
    //Mobile Nav close and redirectig for now
    }

    otherPage() {
      this.homepage = false;
    }
    homePageNav() {
			this.homepage = true;
    }
    redirectMethod() {
        if (this.router.url.includes('customer')) {
            this.router.navigate(['/customerlogin']);
        } else if (this.router.url.includes('candidate')) {
            this.router.navigate(['/candidatelogin']);
        } else {
            this.router.navigate(['/layout']);
        }
    }

}
