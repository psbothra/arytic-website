import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppService } from 'src/app/app.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { NgxSpinnerService } from 'ngx-spinner';
import { UsersList } from 'src/models/Pricing';
@Component({
  selector: 'users-list',
  templateUrl: './users-list.component.html',
  providers: [AppService],
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {
Users:UsersList[]=[];
selected:number=0;
AccessCode:number;
basiclogin:boolean=false
  constructor( private route: ActivatedRoute, private fb: FormBuilder, private toastr:ToastrManager,
    private router: Router, private appService: AppService, private spinner: NgxSpinnerService)
    {

    }

  ngOnInit() {
    this.GetUsers(0);
  }

  filterDate(val)
  {
    this.selected=val;
    this.GetUsers(val);
  }

  login()
  {
   if(this.AccessCode==2510)
   {
     this.basiclogin = true;
   }
   else
   {
     this.basiclogin = false;
   }
  }

  GetUsers(val)
  {
    this.spinner.show();
      this.appService.GetUsers(val)
      .subscribe(
        data => {
          this.Users = data;
          this.spinner.hide();
        });
  }

}
