import { Component, OnInit } from '@angular/core';
import { SeoService } from '../../../shared/SeoService/seo.service'
declare var $:any;
@Component({
  selector: 'Collision',
  templateUrl: './collision.component.html',

})
export class CollisionComponent implements OnInit {

  constructor(private seo: SeoService) { }

  ngOnInit() {

    var url = $("#ourVideo").attr('src');

    $("#videoModalCenter").on('hide.bs.modal', function () {
      $("#ourVideo").attr('src', '');
    });

    $("#videoModalCenter").on('show.bs.modal', function () {
      $("#ourVideo").attr('src', url);
    });

    this.seo.generateTags({
      title: 'Arytic exhibiting at COLLISION, Toronto-Canada World Technology Trade Show',
      description: 'The Collision Conference, one of the largest gatherings of entrepreneurs on the continent, is often described as the fastest growing technology. ',
      image: '../../../assets/images/collision/arytic-news-collision-2.jpg',
      slug: ''
    })
  }

}