import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
declare var $:any;
@Component({
    selector: 'newsandevents',
    templateUrl: './news-and-events.component.html',
})
export class newsandeventsComponent implements OnInit {

    constructor(private route: ActivatedRoute, private router: Router) { }
    ngOnInit() {
        /* getting-url-hash-location */
      var hash_value = window.location.hash;
      if(hash_value){
        var hash_id = hash_value+".hidden-blog";
        $(".hide-cards").hide();
        $(hash_id).show();
      }
      /* #getting-url-hash-location */
        var url = $("#ourVideo").attr('src');
          
        $("#videoModalCenter").on('hide.bs.modal', function(){
            $("#ourVideo").attr('src', '');
        });
        
        $("#videoModalCenter").on('show.bs.modal', function(){
            $("#ourVideo").attr('src', url);
        });
        
$('.card-body .news-view').click(function(){
    $('.hide-cards').hide();
    var t = $(this).attr('news-tab');
    $('.hidden-blog').hide();
    $('#'+ t ).show();
});
$(".goback").click(function(){
    $('.hidden-blog').hide();
    $('.hide-cards').show();
})


    }
    // OpenSBDC()
    // {
    //     this.router.navigateByUrl("newsandevents/SBDC")
    // }
    // OpenSXSW()
    // {
    //     this.router.navigateByUrl("newsandevents/SXSW")
    // }
    // OpenCollision()
    // {
    //     this.router.navigateByUrl("newsandevents/Collision")
    // }
}