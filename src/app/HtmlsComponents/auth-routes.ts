import { Routes } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {AboutComponent} from './aboutus/about.component';
import {LayoutComponent} from './layout.component';
import {FeatureAIHiringPredictorComponent}from './Feature-AIHiringPredictor/Feature-AIHiringPredictor.component';
import {candidateComponent} from './candidate/candidate.component';
import {customerComponent} from './customer/customer.component';
import {contactComponent} from './contactus/contactus.component';
import {disclaimerComponent} from './disclaimer/disclaimer.component';
import { featureSearchComponent } from './Feature-search/Feature-search.component';
import { FeatureHiringBonusComponent } from './Feature-HiringBonus/Feature-HiringBonus.component';
import { FeatureAssessandRepostComponent } from './Feature-AssessandRepost/Feature-AssessandRepost.component';
import { FeatureAnalyticallyGuidedPostComponent } from './Feature-AnalyticallyGuidedPost/Feature-AnalyticallyGuidedPost.component';
import { FeatureImpeccableProfileComponent } from './Feature-ImpeccableProfile/Feature-ImpeccableProfile.component';
import { FeatureIntelligentJobMatchMachineComponent } from './Feature-IntelligentJobMatchMachine/Feature-IntelligentJobMatchMachine.component';
import { FeatureManageBackgroundverificationReportsComponent } from './Feature-ManageBackgroundverificationReports/Feature-ManageBackgroundverificationReports.component';
import { FeatureMLRAInteractiveComponent } from './Feature-MLRA-Interactive-Application/Feature-MLRA-Interactive-Application.component';
import { FeatureNEXTGENInterviewPlatformComponent } from './Feature-NEXTGENInterviewPlatform/Feature-NEXTGENInterviewPlatform.component';
import { FeatureNotaDashboardComponent } from './Feature-NotaDashboard/Feature-NotaDashboard.component';
import { FeatureNotaJobSearchComponent } from './Feature-NotaJobSearch/Feature-NotaJobSearch.component';
import { FeatureNotJustaPostComponent } from './Feature-NotJustaPost/Feature-NotJustaPost.component';
import { FeaturePersonalityTestComponent } from './Feature-PersonalityTest/Feature-PersonalityTest.component';
import { FeaturePowerofDataScienceComponent } from './Feature-PowerofDataScience/Feature-PowerofDataScience.component';
import { FeatureRealTimeDataComponent } from './Feature-RealTimeData/Feature-RealTimeData.component';
import { FeatureSocialReferralPlatformComponent } from './Feature-SocialReferralPlatform/Feature-SocialReferralPlatform.component';
import { FeatureSuggestedJobsComponent } from './Feature-SuggestedJobs/Feature-SuggestedJobs.component';
import { FeatureTransparentJobComponent } from './Feature-TransparentJob/Feature-TransparentJob.component';
import { FeatureUniqueExperiencePlatformComponent } from './Feature-UniqueExperiencePlatform/Feature-UniqueExperiencePlatform.component';
import { FeatureUniqueProfileComponent } from './Feature-UniqueProfile/Feature-UniqueProfile.component';
import { FeatureVideoSizzleComponent } from './Feature-VideoSizzle/Feature-VideoSizzle.component';
import { featuresdetailComponent } from './features-detail/features-detail.component';
import { PrivacyPolicyComponent } from './PrivacyPolicy/PrivacyPolicy.component';
import { insightsComponent } from './insights/insights.component';
import { insightsdetailedComponent } from './insights-detailed-page/insights-detailed-page.component';
import { newsandeventsComponent } from './news-and-events/news-and-events.component';
import { pricingComponent } from './pricing/pricing.component';
import { underconstructionComponent } from './under-construction/under-construction.component';
import {CollisionComponent} from './news-and-events/Arytic-Collision/collision.component';
import {SBDCComponent } from './news-and-events/Arytic-SBDC/SBDC.component';
import {SXSWComponent } from './news-and-events/Arytic.SXSW/SXSW.component';
import {FreelancerecruitersComponent} from './Freelance-Recruiters/freelance-recruiters.component';
import {HiringagencysComponent} from './Hiring-Agency/hiringagency.component';
import {UsersListComponent} from './users-list/users-list.component';
// import {AIComponent} from './insights/Artificial Intelligence/AI.component';
// import {BigdataComponent} from './insights/Bigdata/Bigdata.component';
// import {BlockchainComponent} from './insights/Blockchain/Blockchain.component';
// import {CybersecurityComponent}from './insights/Cybersecurity/Cybersecurity.component'
// import {IoTComponent} from './insights/IoT/IoT.component';


export const AuthRoutes: Routes = [
    { 
        path: '', 
        component: LayoutComponent,
        children: [
          { path: '', component: HomeComponent, pathMatch: 'full'},
          { path: 'home', component: HomeComponent },
          { path: 'aboutus', component: AboutComponent },
          { path: 'Feature-AIHiringPredictor', component: FeatureAIHiringPredictorComponent },
          { path: 'candidate', component:candidateComponent },
          { path: 'customer', component:customerComponent },
          { path: 'contactus', component:contactComponent },
          { path: 'HiringAgencies', component: HiringagencysComponent },
          {path:'Freelancerecruiters',component:FreelancerecruitersComponent},
          { path : 'disclaimer' , component: disclaimerComponent},
          { path: 'PrivacyPolicy', component: PrivacyPolicyComponent },
          { path: 'insights', component: insightsComponent },
          { path: 'insightsdetailed', component: insightsdetailedComponent },
          { path: 'newsandevents/Collision', component: CollisionComponent },
          { path: 'newsandevents', component: newsandeventsComponent,
          
            children: [
              { path: '', redirectTo: 'newsandevents', pathMatch: 'full' },
             
             { path: 'Collision', component: CollisionComponent },
             { path: 'SBDC', component: SBDCComponent },
             { path: 'SXSW', component: SXSWComponent }
           ]
          },
          { path: 'pricing', component: pricingComponent },
          { path: 'underconstruction', component: underconstructionComponent },
          { path: 'FeatureNotaDashboard', component: FeatureNotaDashboardComponent },
          { path: 'FeatureNotaJobSearch', component: FeatureNotaJobSearchComponent },
          { path: 'FeatureNotJustaPost', component: FeatureNotJustaPostComponent },
          { path: 'FeaturePersonalityTest', component: FeaturePersonalityTestComponent },
          { path: 'FeaturePowerofDataScience', component: FeaturePowerofDataScienceComponent },
          { path: 'FeatureRealTimeData', component: FeatureRealTimeDataComponent },
          { path: 'FeatureSocialReferralPlatform', component: FeatureSocialReferralPlatformComponent },
          { path: 'FeatureSuggestedJobs', component: FeatureSuggestedJobsComponent },
          { path: 'FeatureTransparentJob', component: FeatureTransparentJobComponent },
          { path: 'FeatureUniqueExperiencePlatform', component: FeatureUniqueExperiencePlatformComponent },
          { path: 'FeatureUniqueProfile', component: FeatureUniqueProfileComponent },
          { path: 'FeatureVideoSizzle', component: FeatureVideoSizzleComponent },
          { path: 'featuresdetail', component: featuresdetailComponent },
          { path: 'FeatureManageBackgroundverificationReports', component: FeatureManageBackgroundverificationReportsComponent },
          { path: 'featureSearch', component: featureSearchComponent },
          { path: 'FeatureMLRAInteractive', component: FeatureMLRAInteractiveComponent },
          { path: 'FeatureAssessandRepost', component: FeatureAssessandRepostComponent },
          { path: 'FeatureImpeccableProfile', component: FeatureImpeccableProfileComponent },
          { path: 'FeatureIntelligentJobMatchMachine', component: FeatureIntelligentJobMatchMachineComponent },
          { path: 'FeatureHiringBonus', component: FeatureHiringBonusComponent },
          { path: 'FeatureNEXTGENInterviewPlatform', component: FeatureNEXTGENInterviewPlatformComponent },
          { path: 'Feature-AnalyticallyGuidedPost', component: FeatureAnalyticallyGuidedPostComponent },
          {path:'users-list',component:UsersListComponent}
        ]    
      },
    //   {path: 'newsandevents', component: newsandeventsComponent ,
     
    // },
    // {path: 'insights', component: insightsComponent ,
    //   children: [
    //      {path: 'AI', component: AIComponent},
    //       {path: 'Cybersecurity', component: CybersecurityComponent},
    //       {path: 'Blockchain', component: BlockchainComponent},
    //       {path: 'Bigdata', component: BigdataComponent},
    //       {path: 'IoT', component: IoTComponent},
    //       {path: 'Collision', component: CollisionComponent},
    //       {path: 'SBDC', component: SBDCComponent},
    //       {path: 'SXSW', component: SXSWComponent},
    //     ]
    // },
];