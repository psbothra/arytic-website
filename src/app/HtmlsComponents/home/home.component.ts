import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppService } from 'src/app/app.service';
declare var $: any;

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers:[AppService]
})
export class HomeComponent implements OnInit {
  demoform: FormGroup;
  res:any;
  show:any = false;
  afterRequest: any = false;
  emailPattern = '^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$';

  
  Images = [['../assets/images/clients/3m.jpg','../assets/images/clients/activant.jpg'], ['../assets/images/clients/alereon-logo.jpg','../assets/images/clients/attorneytexas.jpg'], ['../assets/images/clients/blue-treble.jpg','../assets/images/clients/cashnet.jpg'],['../assets/images/clients/centaur.jpg','../assets/images/clients/clearorbit-logo.jpg'],['../assets/images/clients/cpsenergy.jpg','./assets/images/clients/crescent.jpg'], ['../assets/images/clients/golfsmith-logo.jpg','../assets/images/clients/gravitant.jpg'], ['../assets/images/clients/h.jpg','./assets/images/clients/harland-clarke.jpg'], ['../assets/images/clients/headspring-logo.jpg','./assets/images/clients/hoovers-logo.jpg'], ['../assets/images/clients/lynda-logo.jpg','./assets/images/clients/maximus-logo.jpg'], ['../assets/images/clients/netspend-logo.jpg','./assets/images/clients/next-logo.jpg'], ['../assets/images/clients/next-logo.jpg','./assets/images/clients/optaros-logo.jpg'], ['../assets/images/clients/planview-logo.jpg','./assets/images/clients/ppd-logo.jpg'], ['../assets/images/clients/pwrf-logo.jpg','./assets/images/clients/quanteqlogo.jpg'], ['../assets/images/clients/raytheon.jpg','./assets/images/clients/reliant-energy.jpg'], ['../assets/images/clients/renewdata-logo.jpg','./assets/images/clients/samsung.jpg'], ['../assets/images/clients/secureinfo.jpg','./assets/images/clients/serviceloyds.jpg'], ['../assets/images/clients/smartsoft.jpg','./assets/images/clients/stdavis-logo.jpg'], ['../assets/images/clients/target.jpg','./assets/images/clients/tasb-logo.jpg'], ['../assets/images/clients/teamandblue.jpg','./assets/images/clients/texas-dads.jpg'], ['../assets/images/clients/texas-dars.jpg','./assets/images/clients/texas-dfps.jpg'], ['../assets/images/clients/texas-dof.jpg','./assets/images/clients/texas-dps.jpg'], ['../assets/images/clients/texas-dshs.jpg','./assets/images/clients/texas-hhsc.jpg'], ['../assets/images/clients/tmf.jpg','./assets/images/clients/trusted-integration.jpg'], ['../assets/images/clients/valero.jpg','./assets/images/clients/weston.jpg']]; 
  SlideOptions = { items: 4, dots: true, nav: false, margin:10, loop:true, autoPlay: 3000}; 
  CarouselOptions = { items: 4, dots: true, nav: false, autoPlay: 3000 };

  optionSlide = { items: 1, dots: true, nav: false, margin: 10, loop: true, autoPlay: 3000 }; 
  employeeSlide = [
    {
      image: '../assets/images/ven-diagram-thumb/thumb_01.jpg',
      title: 'Save Time and Money',
      descritpition:'Current clients are seeing a 74% reduction in Time to Hire and savings of up to $4K per hire.'
    },
    {
      image: '../assets/images/ven-diagram-thumb/thumb_02.jpg',
      title: 'Build relationships with candidates',
      descritpition:'Communicate post status with a simple click instead of having to call or email each candidate.'
    },
    {
      image: '../assets/images/ven-diagram-thumb/thumb_03.jpg',
      title: 'See Verified Information',
      descritpition:'Check background, references, and skills through Arytic, also see what managers and coworkers say about their culture fit.'
    },
    {
      image: '../assets/images/ven-diagram-thumb/thumb_04.jpg',
      title: 'Promote social justice',
      descritpition:'Build a more inclusive team and avoid unconscious bias by seeing applications from qualified candidates without regards to their identity.'
    },
    {
      image: '../assets/images/ven-diagram-thumb/thumb_05.jpg',
      title: 'Crowdsource your post',
      descritpition:'Encourage your social circle to share your job posts to increase visibility to passive candidates.'
    },
    {
      image: '../assets/images/ven-diagram-thumb/thumb_06.jpg',
      title: 'Access job post reviews',
      descritpition:'Improve your jobs posts through candidate feedback to get more applicants.'
    }
  ]

  candidateSlide = [
    {
      image: '../assets/images/ven-diagram-thumb/thumb_07.jpg',
      title: 'Level playing field',
      descritpition:'Showcase your experience without fear of facing bias based on your unique identities.'
    },
    {
      image: '../assets/images/ven-diagram-thumb/thumb_08.jpg',
      title: 'Optimize your opportunities',
      descritpition:'Immediate feedback allows you to spend your time efficiently and apply to the right jobs.'
    },
    {
      image: '../assets/images/ven-diagram-thumb/thumb_09.jpg',
      title: 'Earn referral bonuses',
      descritpition:'Make money when you accept a job offer through Arytic or by referring friends to job posts.'
    },
    {
      image: '../assets/images/ven-diagram-thumb/thumb_10.jpg',
      title: 'Get an insider look',
      descritpition:'See what current and previous employees are saying about the position before you apply.'
    },
    {
      image: '../assets/images/ven-diagram-thumb/thumb_11.jpg',
      title: 'Improve your chances of getting hired',
      descritpition:'Perfect your Arytic Score through recruiter feedback and automated tips.'
    }
  ]

  agnciesSlide = [
    {
      image: '../assets/images/ven-diagram-thumb/thumb_15.jpg',
      title: 'Place more candidates',
      descritpition:'See job posts from all employers and instantly see which jobs to pursue for each candidate.'
    },
    {
      image: '../assets/images/ven-diagram-thumb/thumb_14.jpg',
      title: 'Keep your candidates in the loop',
      descritpition:'Easily send feedback when hiring managers update job status'
    },
    {
      image: '../assets/images/ven-diagram-thumb/thumb_13.jpg',
      title: 'Keep more money',
      descritpition:'Pay a very low monthly fee and a 7% rate for each successful hire.'
    },
    {
      image: '../assets/images/ven-diagram-thumb/thumb_12.jpg',
      title: 'Eliminate busy work',
      descritpition:'Spend more time nurturing your candidate and employer relationships by letting Arytic do the busy work'
    }
  ]
  
  constructor( private route: ActivatedRoute, private fb: FormBuilder, 
    private router: Router, private appService: AppService) {
  
}



Demo()
{
  this.show = false;
  this.afterRequest= false;
  this.ngOnInit();
}

EmailClear()
{
  this.show = false;
}

  ngOnInit()
  {
    this.show = false;
    this.afterRequest= false;
    this.demoform = this.fb.group({
      'GuestUserId':[0, Validators.compose([Validators.required])],
      'FirstName': ['', Validators.compose([Validators.required])],
      'LastName': ['', Validators.compose([Validators.required])],
      'EmailId': ['', Validators.compose([Validators.required, Validators.email])],
      'PhoneNumber': ['',  Validators.compose([Validators.nullValidator, Validators.minLength(10)])],
      'IsDemoRequest': [true, Validators.compose([Validators.nullValidator])],
      'GuestRoleId': ['', Validators.compose([Validators.nullValidator])],
    });
    
    $(function () {
      $(document).ready(function(){

        $('.owl-dots .owl-dot').each(function () {
          $(this).find('span').html('dots');
        });

        $('.diagram__col .ven__icon').each(function(){
          $(this).mouseenter(function() {
            var dataId = $(this).attr('data-id');
            $('.bullet__listing').hide();
            $('#'+dataId).show();
            
          });
          $(this).mouseleave(function() {
            var dataId = $(this).attr('data-id');
            $('#'+dataId).hide();
            $('.bullet__listing').show();
          });
        });
      });
    })
  }

  cancelRequest()
  {
    this.show = false;
    this.afterRequest= false;
    this.demoform.reset();
  }




  DemoRequest()
  {
    this.show = false;
    this.afterRequest= false;
    if(!this.demoform.valid)
    {
      this.demoform.controls['FirstName'].markAsTouched()
      this.demoform.controls['LastName'].markAsTouched()
      this.demoform.controls['EmailId'].markAsTouched()
    }
    else
    {
    this.appService.DemoRequest(this.demoform.value)
    .subscribe(
      data => {
        this.res = data;
        if(this.res == 0 )
        {
          this.afterRequest=true;
        }
        else if (this.res == 1 )
        {
          this.show = true;
        }
      },
      error => {
        this.demoform.reset();
      },
      () => console.log('Call Sucessfull')
    );
    }
  }

}