// import { NgModule } from '@angular/core';
// import { Routes, RouterModule } from '@angular/router';

// const routes: Routes = [];

// @NgModule({
//   imports: [RouterModule.forRoot(routes)],
//   exports: [RouterModule]
// })
// export class AppRoutingModule { }

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppRoutes } from './app-routes';
import { AuthGuard } from './shared/Guard/auth.guard';

@NgModule({
    imports: [
        RouterModule.forRoot(AppRoutes)
    ],
    declarations: [],
    exports: [
        RouterModule
    ],
    providers: [AuthGuard]
})

export class AppRoutesModule {
    constructor() {
    }
}
