import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SeoService} from '../shared/SeoService/seo.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [SeoService],
  declarations: [], 
  exports: [] 
})
export class SharedModule { }