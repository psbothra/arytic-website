import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import {
  Router,
  Event as RouterEvent,
  NavigationStart,
  NavigationEnd,
  NavigationCancel,
  NavigationError
} from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [Title]
})
export class AppComponent implements OnInit {
  title = 'hybridCandidate';
  
  constructor(private router: Router, private titleService: Title,private spinner: NgxSpinnerService) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        (window as any).gtag('config', 'UA-135960440-1', {
          page_title : this.titleService.getTitle(),
          page_path: event.urlAfterRedirects
        });
      }
    });

    router.events.subscribe((event: RouterEvent) => {
      this.navigationInterceptor(event)
    })
  //   router.events.distinctUntilChanged((previous: any, current: any) => {
  //     if(current instanceof NavigationEnd) {
  //         return previous.url === current.url;
  //     }
  //     return true;
  // }).subscribe((x: any) => {
  //     console.log('router.change', x);
  //     ga('send', 'pageview', x.url);
  // });
  }
  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.spinner.show();
    }
    if (event instanceof NavigationEnd) {
      this.spinner.hide();
    }

    // Set loading state to false in both of the below events to hide the spinner in case a request fails
    if (event instanceof NavigationCancel) {
      this.spinner.hide();
    }
    if (event instanceof NavigationError) {
      this.spinner.hide();
    }
  }
  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
          return;
      }
      window.scrollTo(0, 0);
  });
  }
}
