import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutesModule} from './app-routing.module';
import { AppComponent } from './app.component';
import { ToastrModule } from 'ng6-toastr-notifications';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {HtmlsModule} from './HtmlsComponents/htmls.module';
import { AppService } from './app.service';
import { CookieService } from 'ngx-cookie-service';
import {NgxMaskModule} from 'ngx-mask';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { NgxSpinnerModule } from 'ngx-spinner';
import { IntlInputPhoneModule } from 'intl-input-phone';
import { SettingsHttpService } from '../settings/settings.http.service';
import { RecaptchaModule } from 'angular5-google-recaptcha';

export function app_Init(settingsHttpService: SettingsHttpService) {
  return () => settingsHttpService.initializeApp();
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutesModule,
    BrowserAnimationsModule,AutocompleteLibModule,IntlInputPhoneModule,
    HtmlsModule,
    ToastrModule.forRoot(),HttpClientModule,
    RecaptchaModule.forRoot({
      siteKey: '6Ld6CWobAAAAAGUdYvl8v1vRb0g6PGzCuaVp8jWB',
  }),
    NgxMaskModule.forRoot(),
    NgxSpinnerModule
  ],
  providers: [HttpClient,{ provide: APP_INITIALIZER, useFactory: app_Init, deps: [SettingsHttpService], multi: true },
  { provide: 'Window', useValue: window }, AppService, CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
