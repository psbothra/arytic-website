import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandLoginComponent } from './cand-login.component';

describe('CandLoginComponent', () => {
  let component: CandLoginComponent;
  let fixture: ComponentFixture<CandLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
