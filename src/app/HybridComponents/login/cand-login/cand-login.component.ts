import { Component, ElementRef, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// import { ApiService, SettingsService, FormsValidationService } from '../../shared';
import { Router, ActivatedRoute } from '@angular/router';
// import { AlertService } from '../../../shared/AlertService/alert.service';
// import swal from 'sweetalert2';
// import { CookieService } from 'angular2-cookie/core';
import { ToastrManager } from 'ng6-toastr-notifications';
import { NgxSpinnerService } from 'ngx-spinner';
import { AppService } from 'src/app/app.service';
import { TokenReceiveClass, TokenSaveClass, TokenSaveClassId } from 'src/models/TokenValidators';
import { ValdiateEmail } from 'src/models/ValdiateEmail';
import { CookieService } from 'ngx-cookie-service';
import { SubscriptionInputs, Register } from '../cust-signup/cust-signup.component';
import { SettingsService } from 'src/settings/settings.service';
import { ProfileDetails } from 'src/models/ProfileDetails';
import { authConfig } from '../../sso.config'
import { OAuthService } from 'angular-oauth2-oidc';
import { JwksValidationHandler } from 'angular-oauth2-oidc-jwks';
declare function myfunction(): any;
declare function callback_uri(arg): any;

declare var $: any;
declare const gapi: any;
declare var FB: any;

@Component({
  selector: 'app-cand-login',
  templateUrl: './cand-login.component.html',
  styleUrls: ['./cand-login.component.css'],
  providers: [AppService, CookieService] // alertservice
})
export class CandLoginComponent implements OnInit, AfterViewInit {


  signUpform: FormGroup;
  sizeid:any;
  puid:any;
  addsubscription = new SubscriptionInputs();
  info = new Register();
  

  profiles = new ProfileDetails();



  
  JobId: any;
  referrerJobForm: FormGroup;
  loginform: FormGroup;
  show = false;
  emailCheck :any;
  userId:any;
  pendingjobId: string | undefined;
  valid= new validate();
  details:any;
  pending = new Pending();
  tokenVal = new TokenReceiveClass();
  saveToken = new TokenSaveClass();
  saveTokenId = new TokenSaveClassId();
  result = new ValdiateEmail();
  public auth2: any;
  callback_uri:string;
  loading = false;
  planId: any;
  ContactFirstName: any;
  ContactEmail: any;
  fbAutherized: boolean = false;
  ContactLastName: any;
  ProfileTitle: string;
  googleAutherized: boolean = false;
  linkedinAuthorized: boolean = false;
  url: string;
  loginstyle(): void {
    this.loading = true;
     }

  constructor(// private alertService: AlertService,,
    private elementRef: ElementRef,
    public toastr: ToastrManager, private fb: FormBuilder, private service: AppService, private route: ActivatedRoute, private router: Router,
    private cookieService: CookieService,  private spinner: NgxSpinnerService, private settingsService: SettingsService,private oauthService:OAuthService) {

            this.oauthService.setStorage(localStorage);
      this.configureSingleSignOn();
      this.createLoginForm();
      /*************************LinkedIn API ******************************* */

      if('http://localhost:4200/candidatelogin'== window.location.href){
      // alert(window.location.href);
    }
    else{

      var url = new URL(window.location.href);
      var code =url.searchParams.get('code');
      var state =url.searchParams.get('state');
      var mailid =url.searchParams.get('mail');
      // this.loginform.controls['username'].setValue( mail);
      // this.loginform.setValue({
      //    username : mail
      //   });
       this.loginform.controls['username'].setValue(mailid);
      this.service.getAccessToken(code).subscribe(
        data => {
          console.log(data);
          this.oauthService.getAccessToken =data.access_token;
            this.service.getUserEmail(data.access_token).subscribe(a=>{
              this.service.getLinkedinUserDada(data.access_token).subscribe(d=>{
                console.log("firstName="+d.firstName.localized.en_US,"lastName="+d.lastName.localized.en_US);
                console.log("emailAddress="+a.elements[0]["handle~"].emailAddress);
                this.ContactFirstName = d.firstName.localized.en_US;
                this.ContactLastName =d.lastName.localized.en_US;
                this.ContactEmail = a.elements[0]["handle~"].emailAddress;
                this.linkedinAuthorized = true;
                this.GetEmailValidate();
              })
            });
        });
         
    }
  /*************************LinkedIn API ******************************* */


                this.route.params.subscribe(params => {
      console.log(params);
      if (params.id !== null && params.id > 0) {
        sessionStorage.setItem('Id', params.id);
      } else if (params.au !== null && params.au > 0) {
        sessionStorage.setItem('Id', params.au);
        this.service.validateUser(parseInt(params.au, 10))
          .subscribe(
            validate => {
              this.activateUser(validate.UserId);
            });
      }
      else if(params.lid !== null && params.lid > 0)
      {
        sessionStorage.setItem('lid',params.lid)
      }
       else if (params.aid !== null && params.aid > 0) {
        this.activateUser(parseInt(params.aid, 10));
        sessionStorage.setItem('UserId',params.aid)
          sessionStorage.setItem('Jobid', params.Jid); 
          localStorage.setItem('SharedInfo',params.ShareInfo);
      } 
      else if (params.PrId != null) {
        localStorage.setItem("PrId", params.PrId);
      }   
       else if (params.refId != null) {
         this.cookieService.set('type', 'candidate');
         sessionStorage.setItem('refId', params.refId);
        // let refid = sessionStorage.getItem('refId');
        // window.location.href = 'http://demo.tenendus.com:1030/quiz;refid=' + refid; //+sessionStorage.setItem('refId')';
        // window.location.href = url;
        // isLoggedin
        // commented because of redirections not there
        // let refid = sessionStorage.getItem('refId');
        // if (refid != null) {
        //   this.service.GetService('ProfileAPI/api/GetQuestionnaireType?uniqueCode=', refid)
        //     .subscribe(
        //       refType => {
        //         if (refType != null) {
        //           // refType==true? show skip Option
        //           //   --false = personal
        //           this.show = refType;
        //         }
        //       });
        // }
      }
    });
                this.referrerJobForm = this.fb.group({
      ReferralId: [0, Validators.compose([Validators.nullValidator])],
      ReferredByUserId: [0, Validators.compose([Validators.required])],
      JobId: [0, Validators.required],
      ReferredToEmail: [null, Validators.compose([Validators.required, Validators.email])],
      ReferralCode: ['', Validators.nullValidator],
      RegisteredEmail: [null, Validators.nullValidator],
      ReferredToUserId: [0, Validators.nullValidator],
      ReferralCount: [0, Validators.nullValidator],
      ReferralStatusId: [1, Validators.nullValidator],
    });
  }
  activateUser(userId: number) {
  debugger

    this.userId = userId;
    this.service.activateUser(userId)
      .subscribe(
        data => {
            this.PendingJobsUser();     
            localStorage.setItem("PrId", "1");  
          // this.alertService.success('User is activated. Please login to continue');
          // alert('User is activated. Please login to continue');
          this.toastr.successToastr('User is activated. Please login to continue.', 'Success!',{
            position: 'bottom-left'
        });
          setTimeout(() => {
            this.toastr.dismissToastr;
           
          }, 3000);
        });
  }
  PendingJobsUser() {
  debugger

   if(this.pendingjobId  !== "undefined")
    {
    this.pending.userId = this.userId;
    this.pending.jobId = this.pendingjobId;
    this.pending.isLiked = null;
    this.pending.isPending = 0;
    this.service.PendingStatus(this.pending)
      .subscribe(
        data => {
          // this.alertService.success('User is activated. Please login to continue');
          // alert('User is activated. Please login to continue');
          this.userId =null;
          this.pendingjobId =null;
        });
      }   
  }
  Skip() {
    this.router.navigateByUrl('References');
  }


  //***************************************Google Login************************************** */
//   public googleInit() {


//     gapi.load('auth2', () => {
//       this.auth2 = gapi.auth2.init({
//         client_id: '64378685372-k7i28ilkekp5adi1t100atjvgimodsnp.apps.googleusercontent.com',
//         cookiepolicy: 'single_host_origin',
//         scope: 'profile email'
//       });
//       this.attachSignin(document.getElementById('googleBtn'));
//     });
//   }
//   public attachSignin(element) {
  

//     this.auth2.attachClickHandler(element, {},
//       (googleUser) => {

//         let profile = googleUser.getBasicProfile();

//         this.ContactFirstName = profile.getName();
//         this.ContactLastName = profile.getName();
//         this.ContactEmail = profile.getEmail();
//         this.googleAutherized = true;

//         this.GetEmailValidate();
//         console.log('Token || ' + googleUser.getAuthResponse().id_token);
//         console.log('ID: ' + profile.getId());
//         console.log('Name: ' + profile.getName());
//         console.log('Image URL: ' + profile.getImageUrl());
//         console.log('Email: ' + profile.getEmail());
//         //YOUR CODE HERE
//       }, (error) => {
//         alert(JSON.stringify(error, undefined, 2));
//       });
//   }
   ngAfterViewInit(){
    myfunction();

 }

  //***************************************Google Login End***************************************/


  //***************************************Facebook Login ***************************************/

  submitLogin(){
  debugger

    console.log("submit login to facebook");
    // FB.login();
    FB.login((response)=>
        {
          console.log('submitLogin',response);
          if (response.authResponse)
          {
            var _this = this;
              FB.api(
              "/"+response.authResponse.userID+"?fields=first_name,last_name,picture.width(500).height(500),birthday,email,gender",
              function (response) {
                if (response && !response.error) {
                  /* handle the result */
                  console.log("User Data : ",response);
                  _this.ContactFirstName = response.first_name;
                  _this.ContactLastName = response.last_name;
                  _this.ContactEmail = response.email;
                  _this.fbAutherized = true;
                  _this.GetEmailValidate();
                }
              });
            //login success
            //login success code here
            //redirect to home page
           }
           else
           {
           console.log('User login failed');
         }
        },{scope: 'email'});
        
        // alert("sadasdassa");  
        // if(this.fbAutherized == true){
        //   this.UserSignUp();
        // }
  }
  //***************************************Facebook Login End***************************************/

  //***************************************Linkedin Login ***************************************/
  configureSingleSignOn(){
    this.oauthService.configure(authConfig);
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    // this.oauthService.loadDiscoveryDocumentAndTryLogin();
    this.oauthService.tryLogin();
    this.oauthService.requestAccessToken
  }
  inLogin(){
    this.oauthService.initImplicitFlow();
    return this.oauthService.getAccessToken();
  }

  //***************************************Linkedin Login End***************************************/



  candlogin() {
  debugger

    // this.alertService.clear();
    this.service.candidateLogin(this.loginform.value)
      .subscribe(
        data => {
          if (data.UserId) {
            sessionStorage.setItem('isLoggedin', JSON.stringify('true'));
            sessionStorage.setItem('userData', JSON.stringify(data));
            sessionStorage.setItem('userId', JSON.stringify(data.UserId));
            sessionStorage.setItem('profileId', JSON.stringify(data.ProfileId));
            console.log("ProfileId-----",data.ProfileId);
            // console.log(data.UserId);
            if (localStorage.getItem('SharedInfo') != null) {
              const jobShareInfo = localStorage.getItem('SharedInfo');
              // let shareArray: string[] = jobShareInfo.split('~');
              // let jobTitle = shareArray[0];
              // let experience = shareArray[1];
              // let location = shareArray[2];
              this.service.getJobId(parseInt(jobShareInfo, 10))
                .subscribe(data2 => {
                  this.JobId = data2;

                  this.referrerJobForm = this.fb.group({
                    ReferralId: [parseInt(jobShareInfo, 10), Validators.compose([Validators.nullValidator])],
                    ReferredByUserId: [null, Validators.compose([Validators.required])],
                    JobId: [this.JobId, Validators.required],
                    ReferredToEmail: [this.loginform.value.UserName, Validators.compose([Validators.required, Validators.email])],
                    ReferralCode: ['', Validators.nullValidator],
                    RegisteredEmail: [this.loginform.value.UserName, Validators.nullValidator],
                    ReferredToUserId: [parseInt(data.UserId, 10), Validators.nullValidator],
                    ReferralCount: [0, Validators.nullValidator],
                    ReferralStatusId: [1, Validators.nullValidator],
                    IsActive:[1, Validators.nullValidator]
                  });
                  this.service.updateReferral(this.referrerJobForm.value)
                    .subscribe(data1 => {
                      console.log('successful');
                    }
                    );
                  this.router.navigateByUrl('myjobs/referred');
                });
            } else {
              this.service.getProfileId(data.UserId)
                .subscribe(
                  userID => {
                    // this.profileId = userID;
                    // sessionStorage.setItem('profileId', JSON.stringify(data.ProfileId));
                  });
              if (data.IsActive === false) {
                // this.alertService.error('Please activate the link to login');
              } else if (sessionStorage.getItem('refId') != null) {

                // this.router.navigateByUrl('References');
                const refid = sessionStorage.getItem('refId');
                this.service.validateManageReference(parseInt(refid, 10))
                  .subscribe(
                    userID => {
                      if (userID === parseInt(sessionStorage.getItem('userId'), 10)) {
                        window.location.href = 'http://demo.tenendus.com:1030/quiz/' + refid;
                      } else {
                        this.router.navigateByUrl('dashboard');
                      }
                    });

                // window.location.href = 'http://localhost:4201/quiz;refId=' + refid;
                // window.location.href = 'http://demo.tenendus.com:1030/quiz;refId=' + refid;

              } else {
                this.router.navigateByUrl('dashboard');
              }
            }
          }
        }, error => {

          // this.alertService.error('please enter valid details');
          // setTimeout(() => {
            /** spinner ends after 5 seconds */
          //   this.alertService.clear();
          // }, 1000);
          this.loginform.reset();
        },
        () => console.log('Call Sucessfull')
      );
    // if (this.loginform.value.UserName === 'admin' && this.loginform.value.Password === '121212') {
    //   sessionStorage.setItem('isLoggedin', JSON.stringify('true'));
    //   this.router.navigateByUrl('dashboard');
    // }
  }
  MissClear() {
  debugger

    // this.alertService.clear();
  }
  SignUp() {
  debugger

    this.router.navigateByUrl('signup');
  }
  forgot() {
  debugger

    this.router.navigateByUrl('forgotPassword');
  }
    /***************************************************Candidate Signup end***********************************/
    GetProfile(Cid)
    {
      debugger
      this.service.getProfileIdBYCID(Cid)
      .subscribe(
        ID => {
           this.profiles = ID;
           this.profiles.Name = ID.Name.toLowerCase();
        });
      }
  
    CandSignUp() {
      debugger
      // if (this.signUpform.invalid) {
      //   this.signUpform.controls.FirstName.markAsTouched();
      //   this.signUpform.controls.LastName.markAsTouched();
      //   this.signUpform.controls.ProfileTitle.markAsTouched();
      //   this.signUpform.controls.Email.markAsTouched();
      //   this.signUpform.controls.MobilePhone.markAsTouched();
      //   this.signUpform.controls.Password.markAsTouched();
      //   this.toastr.errorToastr('Please provide the valid details!!', 'Oops!',{
      //     position: 'bottom-left'
      // });
      //   setTimeout(() => {
      //       this.toastr.dismissToastr;
      //   }, 3000);
  
      // }
      // else 
      if(this.result.UserId>0)
      {  
        this.show = true;    
      }
       else if(this.result.UserId==0)
       {
      //this.signUpform.value.ProfileTitle= this.ProfileTitle;
      // this.signUpform.value.UserName = this.signUpform.value.Email;
      this.signUpform.value.FirstName=this.ContactFirstName;
      this.signUpform.value.LastName=this.ContactLastName;
      this.signUpform.value.UserName=this.ContactEmail;
      this.signUpform.value.Email=this.ContactEmail;
      this.signUpform.value.IsFaceBookUser=this.fbAutherized;
      this.signUpform.value.IsLinkedInUser=this.linkedinAuthorized;
      this.signUpform.value.IsGooglePlusUser=this.googleAutherized;
      if(this.signUpform.value.UserName == this.profiles.Name && this.profiles.ProfileId > 0)
      {
        this.signUpform.value.Ccpid = sessionStorage.getItem('sid') != null ? parseInt(sessionStorage.getItem('sid'), 10) : 0;
        this.signUpform.value.ExitPid = sessionStorage.getItem('sid') != null ? parseInt(sessionStorage.getItem('sid'), 10) : 0;
      }
      else
      {
        this.signUpform.value.Ccpid =0;
        this.signUpform.value.ExitPid=0;
      } 
      this.signUpform.value.Jid = sessionStorage.getItem('JId') != null ? parseInt(sessionStorage.getItem('JId'), 10) : 0;
      this.signUpform.value.IsActive = sessionStorage.getItem('sid') != null  ? true : false;
      // if(this.signUpform.value.UserName == this.profiles.Name && this.profiles.ProfileId > 0)
      // {
      //   this.signUpform.value.ExitPid = sessionStorage.getItem('sid') != null ? parseInt(sessionStorage.getItem('sid'), 10) : 0;
      // }
      // else
      // {
      //   this.signUpform.value.ExitPid = 0;
      // }  
      this.signUpform.value.ShareInfo = localStorage.getItem('SharedInfo') != null ? parseInt(localStorage.getItem('SharedInfo'), 10) : 0;
      this.signUpform.value.HomeAreaCode='+1';
      debugger
      if(this.fbAutherized || this.linkedinAuthorized||this.googleAutherized){
        this.signUpform.value.password ="";
      }
      this.service.candidateSignUp(this.signUpform.value)
        .subscribe(
          data => {
            debugger
            this.loginform.value.username = this.signUpform.value.Email;
            this.loginform.value.password = this.signUpform.value.Password;
            // this.loginform.valid = true;
            console.log(this.loginform.valid);
            sessionStorage.removeItem('JId');
            if (sessionStorage.getItem('sid') != null) {
              if(this.signUpform.value.UserName == this.profiles.Name && this.profiles.ProfileId > 0)
              {
              this.login(this.loginform);
              }
          } 
          this.login(); 

            // sessionStorage.removeItem('Id'); 
            // this.router.navigateByUrl('login');
          },
          error => {
            this.loading = false;
            this.toastr.errorToastr('Please provide the valid details!!', 'Oops!',{
              position: 'bottom-left'
          });
            setTimeout(() => {
                this.toastr.dismissToastr;
            }, 3000);
            this.ProfileTitle == '';
            this.signUpform.reset();
          },
          () => console.log('Call Sucessfull')
        );
        }
    }
    /***************************************************Candidate Signup end***********************************/
  
  /***************************************************Customer Signup end***********************************/

  GetEmailValidate() {
    this.show = false;
    this.service.validateemail(this.ContactEmail)
    .subscribe(
    data => {
      this.result = data;
      if(this.result.UserId>0)
      {  
        debugger
        if(this.fbAutherized || this.linkedinAuthorized||this.googleAutherized){
         this.loginform.value.username = this.ContactEmail;
         this.loginform.value.password = "";
         this.login(this.loginform); 
        }else{
          this.login(); 
        }
         //Already register with this email Id     
      }else{
        debugger
        this.CandSignUp();
      }
    })
  }

  public UserSignUp() { 
    debugger
 
     if(this.result.UserId>0)
    {  
      this.show = true;    
    }
     else if(this.result.UserId==0)
     {
      this.show = false;
      // this.signUpform.value.UserRoleId= this.selectedOption.id;
      this.signUpform.value.CompanySizeId= this.sizeid?this.sizeid:1;
      this.signUpform.value.CountryCode='+1';
      this.signUpform.value.ContactFirstName=this.ContactFirstName;
      this.signUpform.value.ContactLastName=this.ContactLastName;
      this.signUpform.value.ContactEmail=this.ContactEmail;
      this.signUpform.value.IsFaceBookUser=this.fbAutherized;
        this.service.customerSignUp(this.signUpform.value)
        .subscribe(
        data => {  
          this.puid= data;       
          if (data !== null) {           
            this.AddSubscription();         
            this.Email(data);
           
       }
      });
    }
}
Email(userId) {
  this.info.FullName = this.signUpform.value.ContactFirstName+" "+this.signUpform.value.ContactLastName;
  this.info.ToEmailId = this.signUpform.value.ContactEmail;
  this.info.ApplicationName = 'Arytic';
  this.info.AppLink = this.settingsService.settings.customerLoginRedirection + ';Uid=' + userId + ';Pid=' + this.planId;
  this.info.ClientLogo = '';
  this.service.SignUpEmail(this.info).subscribe(data => { 
      sessionStorage.removeItem('planId')
      this.signUpform.reset();
 
    });
}
AddSubscription()
{
  this.addsubscription.customerId = this.puid;
  this.addsubscription.firstName = this.signUpform.value.ContactFirstName;
  this.addsubscription.lastName = this.signUpform.value.ContactLastName;
  this.addsubscription.eMail = this.signUpform.value.ContactEmail;
  if(this.planId == "1")
  {
    this.addsubscription.PlanId = 'starterdirect'; 
  }
  if(this.planId == "2")
  {
    this.addsubscription.PlanId = 'basicdirect'; 
  }
  if(this.planId == "3")
  {
    this.addsubscription.PlanId = 'growthdirect'; 
  }
  else if(this.planId == 1 || this.planId == undefined)
  {
    this.planId=1;
    this.addsubscription.PlanId = 'starterdirect'; 
  }
  this.service.AddSubscription(this.addsubscription).subscribe(data => {
    if(data == true)
    {    
      this.addsubscription= new SubscriptionInputs();     
    }
    
  });
}
// popupCenter(url, title, w, h) {
//   let left = (screen.width / 2) - (w / 2);
//   let top = (screen.height / 2) - (h / 2);
//    var newWindow = window.open();
//   return newWindow.document.write(url);
// }
// GoogleLogin(){
//   console.log("a");
//   this.service.GoogleLogin().subscribe(
//     data => {
//       let mywindow = this.popupCenter(data, 'Login ', 800, 500);
//       // mywindow.focus();
//     });
// }

// FacebookLogin(){
//   console.log("a");
//   this.service.FacebookLogin().subscribe(
//     data => {
//       let mywindow = this.popupCenter(data, 'Login ', 800, 500);
//       // mywindow.focus();
//     });
// }
    /***************************************************Customer Signup end***********************************/
PostonFB(){
    console.log("response");
    FB.api(
    '/113417937036331/feed?access_token=EAAHtNXmixBQBAGIbhQvC5njx6bbyO9ZC5w6sxrEiKgDWgtDf4uae6izt3HTzdGN1AcLEYtR0jIuAtUW7aq98sf5F7hbmWH3KCGdNNpLeF2IzjwAaEZB7ZB3RYkWkDwy3wlLwOobLZB4UkV30ldUZAjfT9fu4dzqARzO2nz5y4rXoYHRps3D70',
    'POST',
    {
      "Caption":"tyring to post on page",
      "message":"tyring to post on page",
    "link":"https://arytic.com/",
  "created_time":new Date().getDate()},
    function(response) {
        // Insert your code here,

        alert(response);
        console.log(response);
    }
  );
}
  ngOnInit() { 
    // ignore this js load 
    var s = document.createElement("script");
    s.type = "text/javascript";
    s.src = "./././assets/js/oneall_script.js";
    this.elementRef.nativeElement.appendChild(s);
    this.callback_uri = this.settingsService.settings.IdentityapiOneAll;
    callback_uri(this.callback_uri);

    
    this.url=this.settingsService.settings.Identityapi;
    this.planId = sessionStorage.getItem('planId')!== null ? parseInt(sessionStorage.getItem('planId'), 10) : 1;


    this.pendingjobId = sessionStorage.getItem('Jobid');
    // this.loginform = this.fb.group({
    //   UserName: ['', Validators.compose([Validators.required])],
    //   Password: ['', Validators.compose([Validators.required])],
    // });

    $('#show_hide_password a').on('click', function(event) {
      event.preventDefault();
      if ($('#show_hide_password input').attr('type') === 'text') {
          $('#show_hide_password input').attr('type', 'password');
          $('#show_hide_password i').addClass( 'fa-eye-slash' );
          $('#show_hide_password i').removeClass( 'fa-eye' );
      } else if ($('#show_hide_password input').attr('type') === 'password') {
          $('#show_hide_password input').attr('type', 'text');
          $('#show_hide_password i').removeClass( 'fa-eye-slash' );
          $('#show_hide_password i').addClass( 'fa-eye' );
      }
  });

  }
  validateEmail()
{
  debugger

  this.service.validateemail(this.loginform.value.username)
  .subscribe(
      val => {
          this.emailCheck = val;
          if (this.emailCheck.UserId == 0 ) 
          {
            this.loading = false;
            this.toastr.errorToastr('Email not registered with Arytic!!', 'Oops!', {
              position: 'bottom-left'
          });
            setTimeout(() => {
                this.toastr.dismissToastr;
            }, 3000);
          }
          if(localStorage.getItem('SharedInfo') != null && localStorage.getItem('SharedInfo') != "undefined") {
            const jobShareInfo = parseInt(localStorage.getItem('SharedInfo'), 10);
            this.service.getJobId(jobShareInfo) 
            .subscribe(res1 => {
              this.JobId = res1; 
              this.referrerJobForm = this.fb.group({
                ReferralId: [parseInt(localStorage.getItem('SharedInfo'), 10), Validators.compose([Validators.nullValidator])],
                ReferredByUserId: [null, Validators.compose([Validators.required])],
                JobId: [this.JobId, Validators.required],
                ReferredToEmail: [this.loginform.value.username, Validators.compose([Validators.required, Validators.email])],
                ReferralCode: ['', Validators.nullValidator],
                RegisteredEmail: [this.loginform.value.username, Validators.nullValidator],
                ReferredToUserId: [this.emailCheck.UserId, Validators.nullValidator],
                ReferralCount: [0, Validators.nullValidator],
                ReferralStatusId: [1, Validators.nullValidator],
                IsActive:[1, Validators.nullValidator]
              }); 
              if(res1>0)
              {
              this.service.updateReferral(this.referrerJobForm.value)
              .subscribe(da1 => {      
                this.login();                  
               this.referrerJobForm.reset();
              });
               }                
                })   
                     
              }   
          else
          {              
           this.login(); 
          }
      }
  );
}
  login(val?) {
  debugger

    if ((val===null || val===undefined) && !this.loginform.valid) {
      this.loading = false;
      this.toastr.errorToastr('Please provide the valid details!!', 'Oops!', {
        position: 'bottom-left'
    });
      setTimeout(() => {
          this.toastr.dismissToastr;
      }, 3000);
    } else {
      if (val != null) {
        this.loginform = val;
      }
      // else {
      //   debugger;
      this.valid.EmailId = this.loginform.value.username;
      this.service.validatetheDiff(this.valid).subscribe(
        data => {
          this.details = data;
          if ((this.details.UserRoleId === 5 && this.details.IsActive === true) || (this.details.UserRoleId === 2 && this.details.IsActive === true)) {
          this.loginform.value.username = this.valid.EmailId.toLowerCase();
          this.service.validateUserByToken(this.loginform.value)
          .subscribe(
              data1 => {
                  //  alert(data);
                  this.tokenVal = data1;
                  this.service.validateemail(this.loginform.value.username)
                      .subscribe(
                          data2 => {
                              this.result = data2;
                              this.spinner.show();
                              if (this.result.UserId > 0 ) {     
                                if(localStorage.getItem('SharedInfo') != null && localStorage.getItem('SharedInfo') != "undefined") 
                                {                                      
                                        localStorage.removeItem('SharedInfo');
                                        sessionStorage.setItem("RefJobId", this.JobId);                                    
                                      //this.router.navigateByUrl('myjobs/referred');                              
                                } 
                                
                                if(sessionStorage.getItem('JId')!= null&&sessionStorage.getItem('JId')!= "undefined")
                                {
                                  this.pendingjobId = sessionStorage.getItem('JId');
                                  this.pending.userId = this.result.UserId;
                                  this.pending.jobId = this.pendingjobId;
                                  this.pending.isLiked = null;
                                  this.pending.isPending = 0;
                                  this.service.PendingStatus(this.pending)
                                    .subscribe(
                                      data => {
                                        // this.alertService.success('User is activated. Please login to continue');
                                        // alert('User is activated. Please login to continue');
                                        this.pendingjobId =null;
                                      });
                                     
                                } 
                                  // this.appService.Login(this.loginform.value)
                                  //     .subscribe(
                                  //         data2 => {
                                  // if (data.IsActive == false) {
                                  //     this.toastr.error('Please activate the link to login!', 'Oops!');
                                  //     setTimeout(() => {
                                  //         this.toastr.dismissToast;
                                  //     }, 3000);
                                  //     this.loginform.reset();
                                  // }
                                  // else {
                                  // this.password = $('#password').val();
                                  // sessionStorage.setItem('oldPassword', JSON.stringify(this.password));
                                  // sessionStorage.setItem('isLoggedin', JSON.stringify('true'));
                                  // sessionStorage.setItem('userData', JSON.stringify(data));
                                  // this.customerId = data.customerId;
                                  // this.userId = data.userId;
                                  if (localStorage.getItem('UserId')!=null)
                                  {
                                    let val = localStorage.getItem('UserId');
                                    this.saveTokenId.UserId = Number(val);
                                    this.saveTokenId.UserToken = this.tokenVal.access_token;
                                    localStorage.removeItem('UserId');
                                    debugger
                                    this.service.tokenSaveById(this.saveTokenId)
                                    .subscribe(
                                        navigate => {
                                      
                                          const ccpid = sessionStorage.getItem('sid') !== null ? parseInt(sessionStorage.getItem('sid'), 10) : 0;                                           
                                          if(ccpid>0)
                                          {
                                          sessionStorage.removeItem('sid');
                                          let curl = (navigate + ';sid=' + ccpid);
                                          window.location.assign(curl);
                                          }
                                          const Jid = sessionStorage.getItem('Jobid') !== null ? parseInt(sessionStorage.getItem('Jobid'), 10) : 0;                                            
                                          if(Jid>0)
                                          {
                                          sessionStorage.removeItem('Jobid');
                                          let jurl = (navigate + ';JId=' + Jid);
                                          window.location.assign(jurl);
                                          }
                                          const IJid = sessionStorage.getItem('JId') !== null ? parseInt(sessionStorage.getItem('JId'), 10) : 0;                                            
                                          if(IJid>0)
                                          {
                                          sessionStorage.removeItem('JId');
                                          let ijurl = (navigate + ';JId=' + IJid);
                                          window.location.assign(ijurl);
                                          }
                                          const RefId = sessionStorage.getItem('RefJobId') !== null ? parseInt(sessionStorage.getItem('RefJobId'), 10) : 0;
                                         if(RefId>0)
                                          {
                                          sessionStorage.removeItem('RefJobId');
                                          let rurl = (navigate + ';RefId=' + RefId);
                                          window.location.assign(rurl);
                                          }
                                          const clid = sessionStorage.getItem('lid') !== null ? parseInt(sessionStorage.getItem('lid'), 10) : 0;                                            
                                          if(clid>0)
                                          {
                                          sessionStorage.removeItem('lid');
                                          let surl = (navigate + ';sid=' + clid);
                                          window.location.assign(surl);
                                          }
                                          const prid = localStorage.getItem("PrId") !== null ? parseInt(localStorage.getItem("PrId"), 10) : 0;                                            
                                          if(prid>0)
                                          {
                                          localStorage.removeItem('PrId');
                                          let spurl = (navigate + ';PrId=' + prid);
                                          window.location.assign(spurl);
                                          }
                                          if(sessionStorage.getItem('Jobid') == "undefined"&&ccpid==0&&RefId==0&&clid==0)  
                                          {
                                              let url = (navigate.toString());
                                              window.location.assign(url);
                                            
                                          } 
                                          else if(ccpid==0&&Jid==0&&RefId==0&&clid==0&&IJid==0&&prid==0)
                                          {
                                             let url = (navigate.toString());
                                             window.location.assign(url);
                                          }
                                          
                                          // alert(navigate);
                                            // this.cookieService.set('cantkn', this.saveToken.UserToken);
                                            // localStorage.setItem('cantkn', this.saveToken.UserToken);
                                            // tslint:disable-next-line:max-line-length
                                          //  window.location.href = 'http://localhost:4201/login?token=' + this.saveToken.UserToken; // navigate.toString();
                                          //window.location.href = clid > 0 ? (navigate + ';sid=' + clid) : navigate.toString();
                                          //window.location.href = Jid > 0 ? (navigate + ';JId=' + Jid) : navigate.toString();
                                          //window.location.href = RefId > 0 ? (navigate + ';RefId=' + RefId) : navigate.toString();
                                          //window.location.href = ccpid > 0 ? (navigate + ';sid=' + ccpid) : navigate.toString();
                                            // this.router.navigateByUrl(navigate);
                                            // this.router.navigate(['http://localhost:4201/login;token=' + this.saveToken.UserToken],
                                            //   {  queryParams: this.saveToken, skipLocationChange: true });

                                            // this.router.navigate([], {queryParams: {page: null}, queryParamsHandling: 'merge'});
                                        }
                                        );
                                  }
                                  else
                                  {
                                    this.saveTokenId.UserId = this.result.UserId;
                                    this.saveTokenId.UserToken = this.tokenVal.access_token;
                                    this.service.tokenSaveById(this.saveTokenId)
                                      .subscribe(
                                          navigate => {
                                        
                                            const ccpid = sessionStorage.getItem('sid') !== null ? parseInt(sessionStorage.getItem('sid'), 10) : 0;                                           
                                            if(ccpid>0)
                                            {
                                            sessionStorage.removeItem('sid');
                                            let curl = (navigate + ';sid=' + ccpid);
                                            window.location.assign(curl);
                                            }
                                            const Jid = sessionStorage.getItem('Jobid') !== null ? parseInt(sessionStorage.getItem('Jobid'), 10) : 0;                                            
                                            if(Jid>0)
                                            {
                                            sessionStorage.removeItem('Jobid');
                                            let jurl = (navigate + ';JId=' + Jid);
                                            window.location.assign(jurl);
                                            }
                                            const IJid = sessionStorage.getItem('JId') !== null ? parseInt(sessionStorage.getItem('JId'), 10) : 0;                                            
                                            if(IJid>0)
                                            {
                                            sessionStorage.removeItem('JId');
                                            let ijurl = (navigate + ';JId=' + IJid);
                                            window.location.assign(ijurl);
                                            }
                                            const RefId = sessionStorage.getItem('RefJobId') !== null ? parseInt(sessionStorage.getItem('RefJobId'), 10) : 0;
                                           if(RefId>0)
                                            {
                                            sessionStorage.removeItem('RefJobId');
                                            let rurl = (navigate + ';RefId=' + RefId);
                                            window.location.assign(rurl);
                                            }
                                            const clid = sessionStorage.getItem('lid') !== null ? parseInt(sessionStorage.getItem('lid'), 10) : 0;                                            
                                            if(clid>0)
                                            {
                                            sessionStorage.removeItem('lid');
                                            let surl = (navigate + ';sid=' + clid);
                                            window.location.assign(surl);
                                            }
                                            const prid = localStorage.getItem("PrId") !== null ? parseInt(localStorage.getItem("PrId"), 10) : 0;                                            
                                            if(prid>0)
                                            {
                                            localStorage.removeItem('PrId');
                                            let spurl = (navigate + ';PrId=' + prid);
                                            window.location.assign(spurl);
                                            }
                                            if(sessionStorage.getItem('Jobid') == "undefined"&&ccpid==0&&RefId==0&&clid==0)  
                                            {
                                                let url = (navigate.toString());
                                                window.location.assign(url);
                                              
                                            } 
                                            else if(ccpid==0&&Jid==0&&RefId==0&&clid==0&&IJid==0&&prid==0)
                                            {
                                               let url = (navigate.toString());
                                               window.location.assign(url);
                                            }
                                            
                                            // alert(navigate);
                                              // this.cookieService.set('cantkn', this.saveToken.UserToken);
                                              // localStorage.setItem('cantkn', this.saveToken.UserToken);
                                              // tslint:disable-next-line:max-line-length
                                            //  window.location.href = 'http://localhost:4201/login?token=' + this.saveToken.UserToken; // navigate.toString();
                                            //window.location.href = clid > 0 ? (navigate + ';sid=' + clid) : navigate.toString();
                                            //window.location.href = Jid > 0 ? (navigate + ';JId=' + Jid) : navigate.toString();
                                            //window.location.href = RefId > 0 ? (navigate + ';RefId=' + RefId) : navigate.toString();
                                            //window.location.href = ccpid > 0 ? (navigate + ';sid=' + ccpid) : navigate.toString();
                                              // this.router.navigateByUrl(navigate);
                                              // this.router.navigate(['http://localhost:4201/login;token=' + this.saveToken.UserToken],
                                              //   {  queryParams: this.saveToken, skipLocationChange: true });

                                              // this.router.navigate([], {queryParams: {page: null}, queryParamsHandling: 'merge'});
                                          }
                                          );
                                        }
                                  // this.router.navigateByUrl('app-dashboardview');
                                  // }
                                  //    },

                                  // error => {
                                  //     this.toastr.error('Please provide the valid details!', 'Oops!');
                                  //     setTimeout(() => {
                                  //         this.toastr.dismissToast;
                                  //     }, 3000);
                                  //     this.loginform.reset();
                                  // },
                                  //     () => console.log('Call Sucessfull')
                                  // );

                              }
                              // else {
                              //     this.toastr.error('Email Not Registered!', 'Oops!');
                              //     setTimeout(() => {
                              //         this.toastr.dismissToast;
                              //     }, 3000);
                              //     this.loginform.reset();
                              // }
                          });



                  // -------------------------------
                  // this.appService.PostService(data.json, '')
                  //     .subscribe(
                  //         navigate => {
                  //             this.router.navigateByUrl(navigate);
                  //         });
              }, error => {
                this.loading = false;
   this.toastr.errorToastr('Please provide the valid details!!', 'Oops!', {
                position: 'bottom-left'
             });
                setTimeout(() => {
                    this.toastr.dismissToastr;
                }, 3000);
                this.createLoginForm();
                this.spinner.hide();
              });
            } else {
              this.loading = false;
              if (this.details.UserRoleId === 4 || this.details.UserRoleId === 6 || this.details.UserRoleId === 7) {
                this.toastr.warningToastr('Please try to login as Employeer!!', 'Oops Not a Job Seeker!!', {
                  position: 'bottom-left'
              });
                setTimeout(() => {
                    this.toastr.dismissToastr;
                }, 3000);
              } else if (this.details.IsActive === false) {
              this.toastr.errorToastr('Please activate the link to login!', 'Oops!', {
                position: 'bottom-left'
            });
              setTimeout(() => {
                  this.toastr.dismissToastr;
              }, 3000);
            }
            }
        });
      // }
        }
      }
createLoginForm() {
 
  this.loginform = this.fb.group({
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
      grant_type: ['password', Validators.compose([Validators.required])]
  });
}
}

export class validate {
  public EmailId: string;
  public MobileNumber: string;
}

export class Pending
{
  public userId:number;
  public jobId: string;
  public isLiked: boolean;
  public isPending: number;
}
