import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AppService } from 'src/app/app.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { TokenReceiveClass, TokenSaveClass } from 'src/models/TokenValidators';
import { ToastrManager } from 'ng6-toastr-notifications';
// import { CookieService } from 'angular2-cookie';
// import { ToastsManager } from 'ng2-toastr';
// import { AlertService } from 'src/app/shared/AlertService/alert.service';
import { ValdiateEmail } from 'src/models/ValdiateEmail';
import { CookieService } from 'ngx-cookie-service';
import { SubscriptionInputs, Register } from '../cust-signup/cust-signup.component';
import { SettingsService } from 'src/settings/settings.service';
 
// import { OAuthService } from 'angular-oauth2-oidc';
// import { JwksValidationHandler } from 'angular-oauth2-oidc-jwks';
import { CustauthConfig } from '../../ssoCustomer.config';

declare var $: any;
declare const gapi: any;
declare var FB: any;
@Component({
  selector: 'app-cust-login',
  templateUrl: './cust-login.component.html',
  styleUrls: ['./cust-login.component.css'],
  providers: [AppService , CookieService]
})
export class CustLoginComponent implements OnInit {
  public auth2: any;

  addsubscription = new SubscriptionInputs();
  signUpform: FormGroup;
  fbAutherized: boolean = false;
  ContactLastName: any;
  ProfileTitle: string;
  googleAutherized: boolean = false;
  linkedinAuthorized: boolean = false;
  ContactFirstName: any;
  ContactEmail: any; 
  sizeid:any;
  puid:any;
  planId: any;
  info = new Register();

  
  loginform: any;
  tokenVal = new TokenReceiveClass();
  saveToken = new TokenSaveClass();
  valid= new validate();
  details:any;
  emailCheck:any;
  result = new ValdiateEmail();
  emailPattern = '^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-z]{2,4}$';
  show = false;
 loading = false;
 loginstyle(): void {
   this.loading = true;
 }
  constructor(public toastr: ToastrManager, private spinner: NgxSpinnerService,private _vcr: ViewContainerRef, private route: ActivatedRoute, private fb: FormBuilder,
              private router: Router, private appService: AppService , private settingsService: SettingsService, private cookieService: CookieService) {
                //this.configureSingleSignOn();
    this.route.params.subscribe(params => {
      if (params.Uid > 0) {
        this.ActivatetheUser(params.Uid);
      }
      if(params.Pid !== null&& params.Pid > 0)
      {
        sessionStorage.setItem('Pid',params.Pid)
      }
    });
     /*************************LinkedIn API ******************************* */

    //  if('http://localhost:4200/customerlogin'== window.location.href){
    //   // alert(window.location.href);
    // }
    // else{
    //   var url = new URL(window.location.href);
    //   var code =url.searchParams.get('code');
    //   var state =url.searchParams.get('state');
    //   this.appService.getCustAccessToken(code).subscribe(
    //     data => {
    //       console.log(data);
    //       this.oauthService.getAccessToken =data.access_token;
    //         this.appService.getUserEmail(data.access_token).subscribe(a=>{
    //           this.appService.getLinkedinUserDada(data.access_token).subscribe(d=>{
    //             console.log("firstName="+d.firstName.localized.en_US,"lastName="+d.lastName.localized.en_US);
    //             console.log("emailAddress="+a.elements[0]["handle~"].emailAddress);
    //             this.ContactFirstName = d.firstName.localized.en_US;
    //             this.ContactLastName =d.lastName.localized.en_US;
    //             this.ContactEmail = a.elements[0]["handle~"].emailAddress;
    //             this.linkedinAuthorized = true;
    //             this.GetEmailValidate();
    //           })
    //         });
    //     });
         
    // }
  /*************************LinkedIn API ******************************* */

}
createLoginForm() {
  this.loginform = this.fb.group({
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
      grant_type: ['password', Validators.compose([Validators.required])]
  });
}
SignUp() {
  this.router.navigateByUrl('signup');
}
validateEmail()
{
  debugger
  this.appService.validateemail(this.loginform.value.username)
  .subscribe(
      val => {
          this.emailCheck = val;
          if (this.emailCheck.UserId == 0 ) 
          {
            this.loading = false;
            this.toastr.errorToastr('Email not registered with Arytic!!', 'Oops!');
            setTimeout(() => {
                this.toastr.dismissToastr;
            }, 3000);
          }
          else
          {
            debugger
           this.login(); 
          }
      }
  );
}
forgot() {
  // this.router.navigateByUrl('ForgotPassword');
  // this.router.navigate(['login', 1, 'abc']);
//   this.router.navigate(['http://myexternalurl'], { queryParams: this.loginform   , skipLocationChange: true});
//   this.router.navigate(['/product-list'], { queryParams: { page: pageNum } });
// }
}
/***************************************************Customer Signup end***********************************/
  //***************************************Google Login************************************** */
  // public googleInit() {


  //   gapi.load('auth2', () => {
  //     this.auth2 = gapi.auth2.init({
  //       client_id: '64378685372-k7i28ilkekp5adi1t100atjvgimodsnp.apps.googleusercontent.com',
  //       cookiepolicy: 'single_host_origin',
  //       scope: 'profile email'
  //     });
  //     this.attachSignin(document.getElementById('googleBtn'));
  //   });
  // }
  // public attachSignin(element) {
  

  //   this.auth2.attachClickHandler(element, {},
  //     (googleUser) => {

  //       let profile = googleUser.getBasicProfile();

  //       this.ContactFirstName = profile.getName();
  //       this.ContactLastName = profile.getName();
  //       this.ContactEmail = profile.getEmail();
  //       this.googleAutherized = true;

  //       this.GetEmailValidate();
  //       console.log('Token || ' + googleUser.getAuthResponse().id_token);
  //       console.log('ID: ' + profile.getId());
  //       console.log('Name: ' + profile.getName());
  //       console.log('Image URL: ' + profile.getImageUrl());
  //       console.log('Email: ' + profile.getEmail());
  //       //YOUR CODE HERE
  //     }, (error) => {
  //       alert(JSON.stringify(error, undefined, 2));
  //     });
  // }
  ngAfterViewInit(){
 
    //this.googleInit();
}

  //***************************************Google Login End***************************************/


  //***************************************Facebook Login ***************************************/

  // submitLogin(){
  // debugger

  //   console.log("submit login to facebook");
  //   // FB.login();
  //   FB.login((response)=>
  //       {
  //         console.log('submitLogin',response);
  //         if (response.authResponse)
  //         {
  //           var _this = this;
  //             FB.api(
  //             "/"+response.authResponse.userID+"?fields=first_name,last_name,picture.width(500).height(500),birthday,email,gender",
  //             function (response) {
  //               if (response && !response.error) {
  //                 /* handle the result */
  //                 console.log("User Data : ",response);
  //                 _this.ContactFirstName = response.first_name;
  //                 _this.ContactLastName = response.last_name;
  //                 _this.ContactEmail = response.email;
  //                 _this.fbAutherized = true;
  //                 _this.GetEmailValidate();
  //               }
  //             });
  //           //login success
  //           //login success code here
  //           //redirect to home page
  //          }
  //          else
  //          {
  //          console.log('User login failed');
  //        }
  //       },{scope: 'email'});
        
  //       // alert("sadasdassa");  
  //       // if(this.fbAutherized == true){
  //       //   this.UserSignUp();
  //       // }
  // }
  //***************************************Facebook Login End***************************************/

  //***************************************Linkedin Login ***************************************/
  // configureSingleSignOn(){
  //   this.oauthService.configure(CustauthConfig);
  //   this.oauthService.tokenValidationHandler = new JwksValidationHandler();
  //   // this.oauthService.loadDiscoveryDocumentAndTryLogin();
  //   this.oauthService.tryLogin();
  //   this.oauthService.requestAccessToken
  // }
  // inLogin(){
  //   debugger
  //   this.oauthService.initImplicitFlow();
  //   return this.oauthService.getAccessToken();
  // }

  //***************************************Linkedin Login End***************************************/


GetEmailValidate() {
  this.show = false;
  this.appService.validateemail(this.ContactEmail)
  .subscribe(
  data => {
    this.result = data;
    if(this.result.UserId>0)
    {  
      debugger
      debugger
      if(this.fbAutherized || this.linkedinAuthorized||this.googleAutherized){
        this.loginform.value.username = this.ContactEmail;
       this.loginform.value.password = "";
      //  this.loginform.valid = true;//({ 'valid': true });
      // this.loginform.status = "VALID";
      // this.loginform.touched = true;
      } 
        this.login(this.loginform); 
       
       //Already register with this email Id     
    }else{
      debugger
      this.UserSignUp();
    }
  })
}

public UserSignUp() { 
  debugger

   if(this.result.UserId>0)
  {  
    this.show = true;    
  }
   else if(this.result.UserId==0)
   {
    this.show = false;
    // this.signUpform.value.UserRoleId= this.selectedOption.id;
    this.signUpform.value.CompanySizeId= this.sizeid?this.sizeid:1;
    this.signUpform.value.CountryCode='+1';
    this.signUpform.value.ContactFirstName=this.ContactFirstName;
    this.signUpform.value.ContactLastName=this.ContactLastName;
    this.signUpform.value.ContactEmail=this.ContactEmail;
    this.signUpform.value.IsFaceBookUser=this.fbAutherized;
    this.signUpform.value.IsLinkedInUser=this.linkedinAuthorized;
    this.signUpform.value.IsGooglePlusUser=this.googleAutherized;
      this.appService.customerSignUp(this.signUpform.value)
      .subscribe(
      data => {  
        this.puid= data;       
        if (data !== null) {           
          this.AddSubscription();         
          this.Email(data);
          this.loginform.value.username = this.signUpform.value.Email;
            this.loginform.value.password = this.signUpform.value.Password;
            this.login(this.loginform);
     }
    });
  }
}
Email(userId) {
this.info.FullName = this.signUpform.value.ContactFirstName+" "+this.signUpform.value.ContactLastName;
this.info.ToEmailId = this.signUpform.value.ContactEmail;
this.info.ApplicationName = 'Arytic';
this.info.AppLink = this.settingsService.settings.customerLoginRedirection + ';Uid=' + userId + ';Pid=' + this.planId;
this.info.ClientLogo = '';
this.appService.SignUpEmail(this.info).subscribe(data => { 
    sessionStorage.removeItem('planId')
    this.signUpform.reset();

  });
}
AddSubscription()
{
this.addsubscription.customerId = this.puid;
this.addsubscription.firstName = this.signUpform.value.ContactFirstName;
this.addsubscription.lastName = this.signUpform.value.ContactLastName;
this.addsubscription.eMail = this.signUpform.value.ContactEmail;
if(this.planId == "1")
{
  this.addsubscription.PlanId = 'starterdirect'; 
}
if(this.planId == "2")
{
  this.addsubscription.PlanId = 'basicdirect'; 
}
if(this.planId == "3")
{
  this.addsubscription.PlanId = 'growthdirect'; 
}
else if(this.planId == 1 || this.planId == undefined)
{
  this.planId=1;
  this.addsubscription.PlanId = 'starterdirect'; 
}
this.appService.AddSubscription(this.addsubscription).subscribe(data => {
  if(data == true)
  {    
    this.addsubscription= new SubscriptionInputs();     
  }
  
});
}
  /***************************************************Customer Signup end***********************************/

login(val?) {
  debugger
  if ((val===null || val===undefined) && !this.loginform.valid) {
    this.toastr.errorToastr('Please provide the valid details!!', 'Oops!');
    setTimeout(() => {
        this.toastr.dismissToastr;
    }, 3000);
  } 
  else {
  debugger

    this.valid.EmailId = this.loginform.value.username;
    this.appService.validatetheDiff(this.valid)
      .subscribe(
        data => {
          this.details = data;
          if((this.details.UserRoleId != 5 &&this.details.IsActive == true))
          {
            this.loginform.value.username = this.valid.EmailId.toLowerCase();
            this.appService.validateUserByToken(this.loginform.value)
            .subscribe(
                data => {                 
                    //  alert(data);
                    this.tokenVal = data;
                    this.appService.validateemail(this.loginform.value.username)
                        .subscribe(
                            data1 => {
                              this.spinner.show();
                                this.result = data1;
                                if (this.result.UserId > 0 && this.result.CustomerId > 0) {
                                    // this.appService.Login(this.loginform.value)
                                    //     .subscribe(
                                    //         data2 => {
                                    // if (data.IsActive == false) {
                                    //     this.toastr.error('Please activate the link to login!', 'Oops!');
                                    //     setTimeout(() => {
                                    //         this.toastr.dismissToast;
                                    //     }, 3000);
                                    //     this.loginform.reset();
                                    // }
                                    // else {
                                    // this.password = $('#password').val();
                                    // sessionStorage.setItem('oldPassword', JSON.stringify(this.password));
                                    // sessionStorage.setItem('isLoggedin', JSON.stringify('true'));
                                    // sessionStorage.setItem('userData', JSON.stringify(data));
                                    // this.customerId = data.customerId;
                                    // this.userId = data.userId;
                                    this.saveToken.UserToken = this.tokenVal.access_token;
                                    this.saveToken.EmailId =  this.loginform.value.username;
                                    this.appService.tokenSave(this.saveToken)
                                        .subscribe(
                                            navigate => {
                                                // alert(navigate);
                                                this.cookieService.set('custkn', this.saveToken.UserToken);
                                                const plid = sessionStorage.getItem('Pid') !== null ? parseInt(sessionStorage.getItem('Pid'), 10) : 0;                                            
                                                if(plid>0)
                                                {
                                                sessionStorage.removeItem('Pid');
                                                let url = (navigate + ';pId=' + plid);
                                                window.location.assign(url);
                                                }
                                                else
                                                {
                                                  window.location.href = navigate.toString();     
                                                }                                                                                    
                                                // this.router.navigateByUrl(navigate);
                                            });
                                    // this.router.navigateByUrl('app-dashboardview');
                                    // }
                                    //    },
  
                                    // error => {
                                    //     this.toastr.error('Please provide the valid details!', 'Oops!');
                                    //     setTimeout(() => {
                                    //         this.toastr.dismissToast;
                                    //     }, 3000);
                                    //     this.loginform.reset();
                                    // },
                                    //     () => console.log('Call Sucessfull')
                                    // );
  
                                }
                                // else {
                                //     this.toastr.error('Email Not Registered!', 'Oops!');
                                //     setTimeout(() => {
                                //         this.toastr.dismissToast;
                                //     }, 3000);
                                //     this.loginform.reset();
                                // }
                            });
  
  
  
                    // -------------------------------
                    // this.appService.PostService(data.json, '')
                    //     .subscribe(
                    //         navigate => {
                    //             this.router.navigateByUrl(navigate);
                    //         });
                }, error => {
                  this.loading = false;
                    this.toastr.errorToastr('Please provide the valid details!!', 'Oops!');
                    setTimeout(() => {
                        this.toastr.dismissToastr;
                    }, 3000);
                    this.createLoginForm();    
                    this.spinner.hide();           
                });
          }         
          else
          {      
            if(this.details.UserRoleId == 5)
            {
              this.loading = false;
              this.toastr.warningToastr('Please try to login as Job Seeker!!', 'Oops Not an Employer!');
              setTimeout(() => {
                  this.toastr.dismissToastr;
              }, 3000);
            } 
            else if(this.details.IsActive == false)
            {
              this.loading = false;
            this.toastr.errorToastr('Please activate the link to login!', 'Oops!');
            setTimeout(() => {
                this.toastr.dismissToastr;
            }, 3000);           
          }
          }
    
   
       
        }

    );
  }
 
    
      //             );
      // var data = "username=" + userName + "&password=" + password + "&grant_type=password";
      // var reqHeader = new HttpHeaders({ 'Content-Type': 'application/x-www-urlencoded', 'No-Auth': 'True' });
      // return this.http.post(this.rootUrl + '/token', data, { headers: reqHeader });
      //     if(!this.loginform.valid)
      //     {
      //       this.toastr.error('Please provide the valid details!', 'Oops!');
      //       setTimeout(() => {
      //           this.toastr.dismissToast;
      //       }, 3000);
      //       this.loginform.reset();
      //     }
      //     else
      //     {
      //         this.appService.validateemail(this.loginform.value.UserName)
      //         .subscribe(
      //         data => {
      //           this.result = data;
      //           if(this.result.UserId>0&&this.result.CustomerId>0)
      //           {
      //             this.appService.Login(this.loginform.value)
      //             .subscribe(
      //             data => {
      //               if (data.IsActive == false) {
      //                 this.toastr.error('Please activate the link to login!', 'Oops!');
      //                 setTimeout(() => {
      //                     this.toastr.dismissToast;
      //                 }, 3000);
      //                 this.loginform.reset();
      //               }
      //               else {
      //               this.password = $("#password").val();
      //               sessionStorage.setItem('oldPassword',JSON.stringify(this.password));
      //               sessionStorage.setItem('isLoggedin', JSON.stringify('true'));
      //               sessionStorage.setItem('userData', JSON.stringify(data));
      //               this.customerId = data.customerId;
      //               this.userId =data.userId;
      //                   this.router.navigateByUrl('app-dashboardview');
      //               }
      //                 },

      //             error => {
      //               this.toastr.error('Please provide the valid details!', 'Oops!');
      //               setTimeout(() => {
      //                   this.toastr.dismissToast;
      //               }, 3000);
      //               this.loginform.reset();
      //             },
      //             () => console.log('Call Sucessfull')
      //             );

      //           }
      //           else
      //           {
      //             this.toastr.error('Email Not Registered!', 'Oops!');
      //             setTimeout(() => {
      //                 this.toastr.dismissToast;
      //             }, 3000);
      //             this.loginform.reset();
      //           }
      //           });
      //       }
      //   }
  }

MissClear() {
  this.show = false;
  // this.alertService.clear();
}

ActivatetheUser(Uid) {
  this.appService.activateUser(Uid).subscribe(
    data => {
    this.toastr.successToastr('Customer is activated. Please login to continue', 'Success');
    setTimeout(() => {
      this.toastr.dismissToastr;
    }, 3000);
    });
}

ngOnInit() {
     /*************************************Facebook  Login   *********************************/
    //  (window as any).fbAsyncInit = function() {
    //   FB.init({
    //     appId      : '242639986794833',
    //     cookie     : true,
    //     xfbml      : true,
    //     version    : 'v3.1'
    //   });
    //   FB.AppEvents.logPageView();
    // };
 
    //   (function(d, s, id){
    //     var js, fjs = d.getElementsByTagName(s)[0];
    //     if (d.getElementById(id)) {return;}
    //     js = d.createElement(s); js.id = id;
    //     js.src = "https://connect.facebook.net/en_US/sdk.js";
    //     fjs.parentNode.insertBefore(js, fjs);
    //   }(document, 'script', 'facebook-jssdk')); 

   /*************************************Facebook  Login  End ******************************** */
/**************************************Customer Signup ***********************************/
this.signUpform = this.fb.group({
  CandidateIdentifier:  ['', Validators.compose([Validators.nullValidator])],
  CustomerId: [0, Validators.compose([Validators.nullValidator])],
  UserId  : [0, Validators.compose([Validators.required])],
  CompanyName: ['', Validators.compose([Validators.required])],
  CompanySizeId  : [1, Validators.compose([Validators.nullValidator])],
  CompanyLogo: ['', Validators.compose([Validators.nullValidator])],
  ContactFirstName: ['', Validators.compose([Validators.required])],
  ContactMiddleName: ['', Validators.compose([Validators.nullValidator])],
  ContactLastName: ['', Validators.compose([Validators.required])],
  CountryCode:['+1',Validators.compose([Validators.nullValidator])],
  ContactNumber: ['',  Validators.compose([Validators.required])],
  ContactEmail   : ['', Validators.compose([Validators.required, Validators.email])],
  Password: ['', Validators.compose([Validators.required])],
  //Password: ['123456789', Validators.compose([Validators.nullValidator])],
  Address1: ['', Validators.compose([Validators.nullValidator])],
  Address2: ['', Validators.compose([Validators.nullValidator])],
  ZipCode: ['56898', Validators.compose([Validators.required])],
  CountryName: ['USA', Validators.compose([Validators.required])],
  StateName: ['texas', Validators.compose([Validators.required])],
  CityName: ['austin', Validators.compose([Validators.required])],
  PreferredContactDate: ['', Validators.compose([Validators.nullValidator])],
  FromTime: ['', Validators.compose([Validators.nullValidator])],
  ToTime   : ['', Validators.compose([Validators.nullValidator])],
  WebSite: ['', Validators.compose([Validators.nullValidator])],
  Description: ['', Validators.compose([Validators.nullValidator])],
  TimeZoneId  : [1, Validators.compose([Validators.required])],
  UserRoleId: ['', Validators.compose([Validators.nullValidator])],
  ObjCompany: ['', Validators.compose([Validators.nullValidator])],
  ObjTimeZone:  ['', Validators.compose([Validators.nullValidator])]
});
/**********************************************************************************************/
  this.show = false;
  this.createLoginForm();
  $("#show_hide_password a").on('click', function(event) {
    event.preventDefault();
    if($('#show_hide_password input').attr("type") == "text"){
        $('#show_hide_password input').attr('type', 'password');
        $('#show_hide_password i').addClass( "fa-eye-slash" );
        $('#show_hide_password i').removeClass( "fa-eye" );
    }else if($('#show_hide_password input').attr("type") == "password"){
        $('#show_hide_password input').attr('type', 'text');
        $('#show_hide_password i').removeClass( "fa-eye-slash" );
        $('#show_hide_password i').addClass( "fa-eye" );
    }
});

}

}

export class validate
{
  public EmailId:string;
  public MobileNumber:string;
}
