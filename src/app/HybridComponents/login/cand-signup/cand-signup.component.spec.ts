import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandSignupComponent } from './cand-signup.component';

describe('CandSignupComponent', () => {
  let component: CandSignupComponent;
  let fixture: ComponentFixture<CandSignupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandSignupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandSignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
