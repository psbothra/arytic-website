import { Component,ViewContainerRef } from '@angular/core';
import{ Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';
import { AppService } from '../../app.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { FormControl } from '@angular/forms';
declare var $: any;
@Component({

  selector: 'ResetPassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css'],
  providers:[AppService]
})
export class ResetComponent {

  Resetform: any;
  customerId:any;
  companyLogo:any;
  password:any;
  ccis:any=0;
  companieslist: any = [];
  myRecaptcha = new FormControl(false);
  userId:any;
  pid:any;
  Id:any;
  cmail:any;
  selectedIndex:any = -1;
  constructor( private route: ActivatedRoute, private toastr:ToastrManager,private _vcr: ViewContainerRef,
      private fb: FormBuilder, private router: Router,private appService: AppService) {
        this.route.params.subscribe(params => {
            console.log(params);
            if (params['pid'] !==null) {
              localStorage.setItem('Pid', params['pid']);
            }
          });
  }

//   login1(username: string, password: string) {
//     return this.http.post<any>('/api/authenticate', { username: username, password: password })
//         .map(user => {
//             // login successful if there's a jwt token in the response
//             if (user && user.token) {
//                 // store user details and jwt token in local storage to keep user logged in between page refreshes
//                 localStorage.setItem('currentUser', JSON.stringify(user));
//             }

//             return user;
//         });
// }
Login()
{
  this.router.navigateByUrl('');
}

checkId(Id,Cd)
{
  // var d = document.getElementById("col__container"+Id);

  // if(this.selectedIndex != Id){
  //   d.className += " listing_box_selected";
  // }
  this.selectedIndex = Id;
  if(Id!=193||Id!=6)
  {
    this.ccis = Id;
   }
   else
   {
    this.ccis = 0;
   }
   this.cmail = Cd;
}

  Send() {
    if(!this.Resetform.valid)
    {
    this.Resetform.controls.Password.markAsTouched();
    this.Resetform.controls.ConfirmPassword.markAsTouched();
    this.toastr.errorToastr('Please provide the valid details!', 'Oops!');
      setTimeout(() => {
          this.toastr.dismissToastr;
      }, 3000);
    }
    else if(this.myRecaptcha.value === false)
    {
      this.toastr.errorToastr('Please provide google capthca!', 'Oops!');
      setTimeout(() => {
        this.toastr.dismissToastr;
      }, 3000);
    }
    else if(this.myRecaptcha.value === true)
    {
      this.Resetform.value.CId = this.ccis;
      if(this.cmail === undefined)
      {
        this.Resetform.value.Email = this.pid;
      }
      else
      {
          this.Resetform.value.Email = this.cmail;
      }
      this.appService.ResetPassword(this.Resetform.value)
      .subscribe(
      data => {
        this.toastr.successToastr('Password changed successfully','Success');
            this.Resetform.reset();
            this.ccis=0;
            this.cmail=undefined;
            localStorage.removeItem('Pid');
            setTimeout(() => {
              this.toastr.dismissToastr;
                this.Login();
              }, 3000);
           }
      );
          }
  }


  GetCompanyDetails(email) {
    this.appService.GetCompanydetailsList(email)
      .subscribe((values) => {
        debugger
          this.companieslist = values;
      });
  }

  GetEmailId(Id) {
    this.appService.GetEmailWithCandId(Id)
      .subscribe((val) => {
        if(val!=null)
        {
          this.appService.validateCheckemail(val.toString())
          .subscribe(
          data2 => {
            if(data2===5 || data2===2)
            {
            this.GetCompanyDetails(val);
            }
            else
            {
              this.companieslist = [];
            }
          })
        }

      });
  }

  ngOnInit() {
    this.pid =  localStorage.getItem('Pid');
    this.GetEmailId(this.pid);
    this.Resetform = this.fb.group({
      'Email': [this.pid, Validators.compose([Validators.nullValidator])],
      'CId': [0, [Validators.nullValidator]],
      'Password': ['', [Validators.required]],
      'ConfirmPassword': ['', [Validators.required]]
    },
      { validator: matchingPasswords('Password', 'ConfirmPassword') });

  }
}

function matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let Password = group.controls[passwordKey];
      let ConfirmPassword = group.controls[confirmPasswordKey];

      if (Password.value !== ConfirmPassword.value) {
        return {
          mismatchedPasswords: true
        };
      }
    }
  }

