import { AuthConfig } from 'angular-oauth2-oidc';

export const authConfig: AuthConfig = {

  // Url of the Identity Provider
//    clientid='86y46pt3q36a6l',
//    redirectUri='http://localhost:4200/',
//    &scope=r_liteprofile%20r_emailaddress%20w_member_social',

//   issuer: 'https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=86y46pt3q36a6l&redirect_uri=http://localhost:4200/&state=fooobar&scope=r_liteprofile%20r_emailaddress%20w_member_social',
//   var a ='https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id={your_client_id}&redirect_uri=https%3A%2F%2Fdev.example.com%2Fauth%2Flinkedin%2Fcallback&state=fooobar&scope=r_liteprofile%20r_emailaddress%20w_member_social'

  // URL of the SPA to redirect the user to after login

  // The SPA's id. The SPA is registerd with this id at the auth-server
//   clientId: '86y46pt3q36a6l',

  // set the scope for the permissions the client should request
  // The first three are defined by OIDC. The 4th is a usecase-specific one
//   scope: 'openid profile email voucher',
  // Url of the Identity Provider
  issuer: 'https://www.linkedin.com/oauth/v2/',

  // Login Url of the Identity Provider
   loginUrl: 'https://www.linkedin.com/oauth/v2/authorization?response_type=code'+"&scope=r_liteprofile+r_emailaddress",
   //&scope=r_liteprofile%20r_emailaddress',
//    loginurl: 'https://demo.identityserver.com/identity/connect/authorize',

  // Login Url of the Identity Provider
//   logouturl: 'https://demo.identityserver.com/identity/connect/endsession',
// customTokenParameters:'https://www.linkedin.com/oauth/v2/',
requestAccessToken:true,

userinfoEndpoint:'https://api.linkedin.com/v1/me',
  // URL of the SPA to redirect the user to after login
//   redirectUri: window.location.origin + '/dashboard.html',
  redirectUri: 'http://localhost:4200/candidatelogin',

  // The SPA's id. The SPA is registerd with this id at the auth-server
  clientId: '86y46pt3q36a6l',

  // set the scope for the permissions the client should request
  // The first three are defined by OIDC. Also provide user sepecific
//   scope: 'openid profile email billing_demo_api',
//   scope: 'r_liteprofile r_emailaddress w_member_social',
  
}