import { Component , ViewContainerRef} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';
import { ToastrManager } from 'ng6-toastr-notifications';
import { AppService } from '../../app.service';

declare var $: any;
@Component({

  selector: 'ForgotPassword',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.css'],
  providers:[AppService]
})
export class ForgotComponent {

  Forgotform: any;
  customerId:any;
  companyLogo:any;
  password:any;
  email:any;
  result :any;
  ForgotPasswordNew=false;
  userId:any;
  constructor( private route: ActivatedRoute, private toastr:ToastrManager,private _vcr: ViewContainerRef,
      private fb: FormBuilder, private router: Router,private appService: AppService) {

  }


  Send() {
    if(!this.Forgotform.valid)
    {
      this.toastr.errorToastr('Please provide the valid details!', 'Oops!');
      setTimeout(() => {
        this.toastr.dismissToastr;
      }, 3000);
    }
    else
    {
      this.appService.validateemail(this.Forgotform.value.EmailId)
      .subscribe(
      data => {
        this.result = data;
        if(this.result.UserId>0 && this.result.UserId!=undefined)
        {
          this.appService.ForgotPassword(this.Forgotform.value)
          .subscribe(
          data => {
            this.toastr.successToastr('Please check your email to reset the password', 'Success!');
                setTimeout(() => {

                    this.toastr.dismissToastr;
                    this.Forgotform.reset();
                    //this.ForgotPasswordNew = true;
                  }, 3000);

               }

          );
        }
        else
        {
          this.toastr.errorToastr('Please check your email provided', 'Email Not Found!');
          setTimeout(() => {

              this.toastr.dismissToastr;
              this.Forgotform.reset();
              //this.ForgotPasswordNew = true;
            }, 3000);

         }

      }
        )

    }


  }


  ngOnInit() {
    this.Forgotform = this.fb.group({
      'EmailId': ['', Validators.compose([Validators.required])],
    });

  }
}

