import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SettingsService } from 'src/settings/settings.service';
declare var $: any; 
@Component({
  
  selector: 'layout',
  templateUrl: './login-layout.component.html',
  styleUrls: ['./login-layout.component.css'],
})
export class LoginLayoutComponent {

  constructor(private router: Router,private settingsService: SettingsService) {

  }
 
  candLogin()
  {
    window.location.href = this.settingsService.settings.candidateLogingRedirection;
  }

  custLogin()
  {
    window.location.href = this.settingsService.settings.customerLogingRedirection;
  }

  ngOnInit() {

    $(".hr-click").click(function(){
    $(".job-c").addClass("sl-show");
    $(".hiring-c").addClass("sl-hide");
    $(".hiring-c").removeClass("sl-show");
  });
  $(".job-click").click(function(){
  $(".hiring-c").addClass("sl-show");
  $(".job-c").addClass("sl-hide");
  $(".job-c").removeClass("sl-show");
});

  }
}

