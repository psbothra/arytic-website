import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { Routes } from '@angular/router';
import { CandLoginComponent } from './login/cand-login/cand-login.component';
import { CustLoginComponent } from './login/cust-login/cust-login.component';
import { CustSignupComponent } from './login/cust-signup/cust-signup.component';
import { CandSignupComponent } from './login/cand-signup/cand-signup.component';
import {LoginLayoutComponent} from './login-layout/login-layout.component';
import {ForgotComponent} from './ForgotPassword/forgot.component';
import { ResetComponent } from './ResetPassword/resetpassword.component';
import { NewCandSignupComponent } from './login/candnew-signup/candnew-signup.component';

const HybridRoutes: Routes = [
    {path: 'layout', component:LoginLayoutComponent},
    { path: 'candidatelogin', component: CandLoginComponent },
    { path: 'customerlogin',  component: CustLoginComponent },
    { path: 'candidatesignup', component: CandSignupComponent },
    { path: 'customersignup', component: CustSignupComponent},
    { path: 'candidatesignuppublic', component: NewCandSignupComponent },
    { path: 'ForgotPassword' , component:ForgotComponent},
    { path: 'ResetPassword' , component:ResetComponent}
];

@NgModule({
    imports: [
        RouterModule.forChild(HybridRoutes), CommonModule
    ],
    declarations: [],
    exports: [
        RouterModule
    ]
})

export class RoutesModule {
    constructor() {
    }
}
