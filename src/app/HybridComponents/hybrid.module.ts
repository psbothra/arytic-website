import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RoutesModule } from './hybrid-routes.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CandLoginComponent } from './login/cand-login/cand-login.component';
import { CustLoginComponent } from './login/cust-login/cust-login.component';
import { CustSignupComponent } from './login/cust-signup/cust-signup.component';
import { CandSignupComponent } from './login/cand-signup/cand-signup.component';
import {LoginLayoutComponent} from './login-layout/login-layout.component';
import {ForgotComponent} from './ForgotPassword/forgot.component';
import { ResetComponent } from './ResetPassword/resetpassword.component';
// import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { ToastrModule } from 'ng6-toastr-notifications';
import { HttpClientModule } from '@angular/common/http';
import { AppService } from '../app.service';
import { CookieService } from 'ngx-cookie-service';
import { NgxSpinnerModule } from 'ngx-spinner';
import {NgxMaskModule} from 'ngx-mask';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import { IntlInputPhoneModule } from 'intl-input-phone';
import {OAuthModule} from 'angular-oauth2-oidc';
import { RecaptchaModule } from 'angular5-google-recaptcha';
import { NewCandSignupComponent } from './login/candnew-signup/candnew-signup.component';
import { NgxDocViewerModule } from 'ngx-doc-viewer';
@NgModule({
  imports: [
    FormsModule,AutocompleteLibModule,NgxSpinnerModule,IntlInputPhoneModule,    RecaptchaModule.forRoot({
      siteKey: '6Ld6CWobAAAAAGUdYvl8v1vRb0g6PGzCuaVp8jWB',
  }),
    RoutesModule, ReactiveFormsModule, CommonModule,  ToastrModule.forRoot(), HttpClientModule,NgxMaskModule.forRoot()// ,HttpModule,
    ,NgxDocViewerModule,OAuthModule.forRoot({ resourceServer: {
      allowedUrls: ['http://localhost:4200/candidatelogin','http://localhost:4200/','http://localhost:4200'],
      sendAccessToken: true}})
  ],
  declarations: [CandLoginComponent, ForgotComponent,ResetComponent,CustLoginComponent, CustSignupComponent, CandSignupComponent,LoginLayoutComponent,NewCandSignupComponent],
  providers: [AppService, CookieService]
})
export class HybridModule { }
