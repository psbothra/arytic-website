import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, of } from "rxjs";
import { map, debounceTime,catchError, tap,retry } from "rxjs/operators";
import { ValdiateEmail } from 'src/models/ValdiateEmail';
import { ValdiateUser } from 'src/models/ValidateUser';
import { ConfigurePassword } from 'src/models/ConfigurePassword';
import {ProfileDetails} from 'src/models/ProfileDetails';
import {PlanFeature,companysize, UsersList} from 'src/models/Pricing';
import { SettingsService } from 'src/settings/settings.service';
 

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http: HttpClient, private settingsService: SettingsService) { }

  validateUserByToken(body) {
    let data = 'username=' + body.username + '&password=' + body.password + '&grant_type=password';
    let reqHeader = new HttpHeaders({ 'Content-Type': 'application/x-www-urlencoded', 'No-Auth': 'True' });
    return this.http.post(this.settingsService.settings.validateUserByToken, data, { headers: reqHeader }).pipe(map((res: any) => res));
    // catchError(<T>(error: any, result?: T) => {
    //   console.log(error);
    //   return of(result as T);
    // }) 
    // .catch((error: any) => {
    //   return Observable.throw(error.json());
    // });
  }
  tokenSave(body) {
     return this.http.post(this.settingsService.settings.tokenSave, body).pipe(
     map((res: Response) => res));
     catchError(<T>(error: any, result?: T) => {
      console.log(error);
      return of(result as T);
    })  
    //  this.http.post(this.settingsService.settings.tokenSave, body).pipe(
    //   retry(3), 
    //   map((res: Response) => {  
    //     return res;
    //   }),
    //   catchError(<T>(error: any, result?: T) =>{
    //     console.error(error);
    //     return of(result as T);
    //   })
    //  )
  }

  tokenSaveById(body) {
    return this.http.post(this.settingsService.settings.tokenSaveId, body).pipe(
    map((res: Response) => res));
    catchError(<T>(error: any, result?: T) => {
     console.log(error);
     return of(result as T);
   })  
   //  this.http.post(this.settingsService.settings.tokenSave, body).pipe(
   //   retry(3), 
   //   map((res: Response) => {  
   //     return res;
   //   }),
   //   catchError(<T>(error: any, result?: T) =>{
   //     console.error(error);
   //     return of(result as T);
   //   })
   //  )
 }
  validateemail(email: string): Observable<ValdiateEmail> {
    const url = this.settingsService.settings.emailVaild + 'email=' + email;
    // return this.http.get<ValdiateEmail>(url).pipe(
    // debounceTime(1000),map((res: any) => res.json())
    // catchError(<T>(error: any, result?: T) => {
    //   console.log(error);
    //   return of(result as T);
    // }); 
    return this.http.get<ValdiateEmail>(url).pipe(
      debounceTime(1000), map(res => res));
    }

  validateUser(statusId: number): Observable<ValdiateUser> {
    const url = this.settingsService.settings.valdiateUser + '?statsId=' + statusId; 
    return this.http.get<ValdiateUser>(url).pipe(
      debounceTime(1000), map(res => res));
  } 

  validatetheDiff(body) :any {
    return this.http.post(this.settingsService.settings.validatetheUser, body).pipe(
    map((res: Response) => res));
    // catchError(<T>(error: any, result?: T) => {
    //   console.log(error);
    //   return of(result as T);
    // }) 
  }

  searchJobTitle(term: string) : Observable<string[]> {
    const url = this.settingsService.settings.jobTitleEndpoint + '?jobtitle=' + term;
    return this.http.get<string[]>(url).pipe(
      debounceTime(1000), map(res => res));
  }
  
  configurePassword(): Observable<ConfigurePassword[]> {
    const url = this.settingsService.settings.configurePassword; 
    return this.http.get<ConfigurePassword[]>(url).pipe(
      debounceTime(1000), map(res => res));
  } 
   activateUser(userId: number) {
    const url = this.settingsService.settings.activateUser + '?userId=' + userId; 
    return this.http.get(url).pipe(
      debounceTime(1000), map(res => res));
  }
  getJobId(referralId: number) {
    const url = this.settingsService.settings.getJobId + '?referralId=' + referralId; 
    return this.http.get(url).pipe(
      debounceTime(1000), map(res => res));
  }
  getProfileId(userId: number) {
    const url = this.settingsService.settings.getProfileId + '?userId=' + userId; 
    return this.http.get(url).pipe(
      debounceTime(1000), map(res => res));
  }
  getProfileIdBYCID(Cpid: number) : Observable<ProfileDetails>  {
    const url = this.settingsService.settings.getProfileIdByCID + '?cCPId=' + Cpid; 
    return this.http.get<ProfileDetails>(url).pipe(
      debounceTime(1000), map(res => res));
  }
  validateManageReference(refId: number) {
    const url = this.settingsService.settings.validateManageReference + '?refId=' + refId; 
    return this.http.get(url).pipe(
      debounceTime(1000), map(res => res));
  }
  updateReferral(body) :any {
    return this.http.post(this.settingsService.settings.updateReferral, body).pipe(
    map((res: Response) => res));
    // catchError(<T>(error: any, result?: T) => {
    //   console.log(error);
    //   return of(result as T);
    // }) 
  }
  customerLogin(body) :any {
    return this.http.post(this.settingsService.settings.customerlogin, body).pipe(
    map((res: Response) => res));
    // catchError(<T>(error: any, result?: T) => {
    //   console.log(error);
    //   return of(result as T);
    // }) 
  }


  /*************************LinkedIn API ******************************* */
  // getloginForm(){
  //   this.oauthService.initImplicitFlow();
  //   return this.oauthService.getAccessToken();
  // }

    getAccessToken(code):any
  { 
  const headerDict = {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Accept': 'application/json',
    'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT'
  }
  const formData = new FormData();
      formData.append('rant_type','authorization_code');
      formData.append('code',code);
      formData.append('redirect_uri','http://localhost:4200/candidatelogin');
      formData.append('client_id','86y46pt3q36a6l');
      formData.append('client_secret','pnYRXqF1JQApR5aq');
      // formData.append('client_secret','pnYRXqF1JQApR5aq');

  const requestOptions = {                                                                                                                                                                                 
    headers: new HttpHeaders(headerDict),  
  };
  var url = 'https://www.linkedin.com/oauth/v2/accessToken?grant_type=authorization_code&code='+code+'&redirect_uri=http://localhost:4200/candidatelogin&client_id=86y46pt3q36a6l&client_secret=pnYRXqF1JQApR5aq&scope=r_liteprofile+r_emailaddress';
  // var url = 'https://www.linkedin.com/oauth/v2/accessToken';
    return this,this.http.get(url);
  }
  getCustAccessToken(code):any
  { 
  const headerDict = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT'
  }

  const requestOptions = {                                                                                                                                                                                 
    headers: new HttpHeaders(headerDict), 
  };
 var url = 'https://www.linkedin.com/oauth/v2/accessToken?grant_type=authorization_code&code='+code+'&redirect_uri=http://localhost:4200/customerlogin&client_id=86y46pt3q36a6l&client_secret=pnYRXqF1JQApR5aq&scope=r_liteprofile+r_emailaddress';
    return this,this.http.get(url);
  }
  getLinkedinUserDada(accessToken):any
  {
    // debugger  

    // let headers =  {headers: new  HttpHeaders({ 
    //   'Content-Type': 'application/x-www-form-urlencoded',
    //   'Access-Control-Allow-Origin': '*',
    //   'Access-Control-Allow-Credentials': 'true',
    //   'Authorization': `Bearer ${accessToken}`,
    //   'mode': 'no-cors'
    // })}; 
    // 'authorization?response_type=code&client_id=86y46pt3q36a6l&redirect_uri=http://localhost:4200/candidatelogin&state=987654321&scope=r_basicprofile,r_emailaddress"'
    return this.http.get('https://api.linkedin.com/v2/me?projection=(id,firstName,lastName,emailAddress,profilePicture(displayImage~:playableStreams))&oauth2_access_token='+accessToken);
  }
  getUserEmail(accessToken):any{
    var headers = {"Authorization": "Bearer "+accessToken,
    "Access-Control-Allow-Origin": "*",
    'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT' }
    const requestOptions = {                                                                                                                                                                                 
      headers: new HttpHeaders(headers), 
    };
    return this.http.get('https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))&oauth2_access_token='+accessToken);

  }

  /*************************LinkedIn API ******************************* */


  AddPlanDetails(body) :any {
    return this.http.post(this.settingsService.settings.AddPlan, body).pipe(
    map((res: Response) => res));
  }

  AddSubscription(body) :any {
    return this.http.post(this.settingsService.settings.AddSubscription, body).pipe(
    map((res: Response) => res));
  }

  EnquireNow(body) :any {
    return this.http.post(this.settingsService.settings.EnquireNow, body).pipe(
    map((res: Response) => res));
  }

  candidateLogin(body) :any {
    return this.http.post(this.settingsService.settings.candidateLogin, body).pipe(
    map((res: Response) => res)); // ,
    // catchError(<T>(error: any, result?: T) => {
    //   console.log(error);
    //   return of(result as T);
    // }) 
  }

  UpdateProfile(ProfileId:number)
  {
    const url = this.settingsService.settings.UpdateProfileCustomer + '?profileId=' + ProfileId; 
    return this.http.get(url).pipe(
      debounceTime(1000), map(res => res));
  }

  validateCheckemail(email: string){
    const url = this.settingsService.settings.UserRoleCheck + "?email=" + email;
    return this.http.get(url).pipe(
      debounceTime(1000), map(res => res));
  }

  GetCompanydetailsList(email:string)
  {
    const url = this.settingsService.settings.GetCompanydetailsList + '?email=' + email; 
    return this.http.get(url).pipe(
      debounceTime(1000), map(res => res));
  }

  GetEmailWithCandId(Id:string)
  {
    const url = this.settingsService.settings.GetEmailWithCandId + '?Id=' + Id; 
    return this.http.get(url).pipe(
      debounceTime(1000), map(res => res));
  }

  PendingStatus(body) :any 
  {
    return this.http.post(this.settingsService.settings.pendingStatus, body).pipe(
      map((res: Response) => res)); 
  }
  ForgotPassword(body) {
    return this.http.post(this.settingsService.settings.forgotPassword, body).pipe(
      map((res: Response) => res)); //,
      // catchError(<T>(error: any, result?: T) => {
      //   console.log(error);
      //   return of(result as T);
      // }) 
  }

  GetPlans(): Observable<PlanFeature[]>
  {
    const url = this.settingsService.settings.GetPlans;
    return this.http.get<PlanFeature[]>(url).pipe(
      debounceTime(1000), map(res => res));
        
  }

  GetUsers(val): Observable<UsersList[]>
  {
    const url = this.settingsService.settings.UsersList + '?Filter=' + val;
    return this.http.get<UsersList[]>(url).pipe(
      debounceTime(1000), map(res => res));
        
  }

  GetCompanySize(): Observable<companysize[]>
  {
    const url = this.settingsService.settings.GetCompanySize;
    return this.http.get<companysize[]>(url).pipe(
      debounceTime(1000), map(res => res));
        
  }



  AryticWhitepaper(body) {
    return this.http.post(this.settingsService.settings.SaveAryticWhitepaper, body).pipe(
      map((res: Response) => res)); //,
      // catchError(<T>(error: any, result?: T) => {
      //   console.log(error);
      //   return of(result as T);
      // }) 
  }

  AryticSubscription(body) {
    return this.http.post(this.settingsService.settings.AryticSubscription, body).pipe(
      map((res: Response) => res)); //,
      // catchError(<T>(error: any, result?: T) => {
      //   console.log(error);
      //   return of(result as T);
      // }) 
  }


  ResetPassword(body) {
    return this.http.post(this.settingsService.settings.resetPassword, body).pipe(
    map((res: Response) => res)); //,
    catchError(<T>(error: any, result?: T) => {
      console.log(error);
      return of(result as T);
    }) 
  }
  SignUpEmail(body) {
    return this.http.post(this.settingsService.settings.EmailInvite, body).pipe(
    map((res: Response) => res));
    // catch((error: any) => {
    //   return Observable.throw(error.json());
    // });
  }

  //Use as a example
//   getRecipes() {
//     this.httpClient
//         .get('https://ng-recipe-book.firebaseio.com/recipes.json')
//         .pipe(
//             map((response: Response) => {
//                 const recipes: Recipe[] = response.json();
//                 for (const recipe of recipes) {
//                     if (!recipe['ingredients']) {
//                         recipe['ingredients'] = [];
//                     }
//                 }
//                 return recipes;
//             }),
//             tap((recipes: Recipe[]) => {
//                 this.recipeService.setRecipes(recipes);
//             })
//         );
// }
  customerSignUp(body) {
    return this.http.post(this.settingsService.settings.customerSignUp, body).pipe(
    map((res: Response) => res));
    // catchError(<T>(error: any, result?: T) => {
    //   console.log(error);
    //   return of(result as T);
    // }) 
  }

  DemoRequest(body) {
    return this.http.post(this.settingsService.settings.DemoRequest, body).pipe(
    map((res: Response) => res));
  }

  candidatePrivateSignUp(body) {
    return this.http.post(this.settingsService.settings.PrivateRegister, body).pipe(
    map((res: Response) => res));
  }

  candidatePublicSignUp(body) {
    return this.http.post(this.settingsService.settings.PublicRegister, body).pipe(
    map((res: Response) => res));
  }



  UpdatePublicProfile(body) {
    return this.http.post(this.settingsService.settings.Updatepublicprofile, body).pipe(
    map((res: Response) => res));
  }
  
  candidateSignUp(body) {
    return this.http.post(this.settingsService.settings.candidateSignUp, body).pipe(
    map((res: Response) => res));
    // catchError(<T>(error: any, result?: T) => {
    //   console.log(error);
    //   return of(result as T);
    // }) 
  }
  private handleError(error: any) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.log(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }
}
