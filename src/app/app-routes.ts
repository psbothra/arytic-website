import { Routes } from '@angular/router';
import { AuthGuard } from './shared/Guard/auth.guard';
import {HomeComponent} from './HtmlsComponents/home/home.component';


export const AppRoutes: Routes = [


    { path: '', loadChildren: './HtmlsComponents/htmls.module#HtmlsModule', canActivate: [AuthGuard] },
    { path: '', loadChildren: './HybridComponents/hybrid.module#HybridModule' },
    { path: '**',  loadChildren: './HtmlsComponents/htmls.module#HtmlsModule'  }

];