(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
'use strict';

(function ($) {

    //Graph tabs on features detail page - features-detail.html
    $('.graph-tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var $currentLi = $(e.target).parent();
        var $ul = $currentLi.closest('ul');
        var $prevLi = $currentLi.prevAll();
        var $nextLi = $currentLi.nextAll();
        var $firstLi = $('.graph-tabs .nav-tabs li')[0];

        $.each($nextLi, function (index, element) {
            $($firstLi).removeClass('first');
            $(element).removeClass('completed');
        });

        $.each($prevLi, function (index, element) {
            $($firstLi).addClass('first');
            $(element).addClass('completed');
        });

        $currentLi.addClass('completed');
    });
})(jQuery);

},{}]},{},[1]);
//$(window).scroll(function() {
    //var nav = $('#arytic-navbar');
    //var top = 100;
    //if ($(window).scrollTop() >= top) {

      //  nav.addClass('fixed-top');

   // } else {
      //  nav.removeClass('fixed-top');
   // }
//});