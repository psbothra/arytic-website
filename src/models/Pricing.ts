export class PlanFeature
{
    plansoffered : Plan[] = [];
    featuresOffered : Features[]  = [];
}


export class Plan
{
 id:number;
 planName:string;
 dailyCharges:string;
}

export class Features
{
    id:number;
    featureName:string;
    price:number; 
    featureComment:string;   
}

export class companysize
{
    CompanySizeId:number;
    CompanySize:string;
}

export class UsersList
{
    UserId: number;
    FullName: string;
    Email: string;
    CreatedOn: string;
    UserRoleId: string;
    CLocation: string;
    MobilePhone: string;
    CompanyName: string;
}