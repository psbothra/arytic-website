
export class TokenReceiveClass {
    public access_token: string;
    public token_type: string;
    public expires_in: string;
}
export class TokenSaveClass {
    public UserToken: string;
    public EmailId: string;
}

export class TokenSaveClassId {
    public UserToken: string;
    public UserId : number;
}
